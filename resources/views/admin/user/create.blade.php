@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
@endpush
@if(isset($user->email))
    @section('pageTitle', 'Editing ' . $user->email)
@else
    @section('pageTitle', 'Add User')
@endif
{{--{{ dd(get_defined_vars()) }}--}}
{{--{{$error}}--}}


@section('content')
    <div class="container">
        <form @if($user->id) action="{{ route('update-promoter', $user->id) }}" @else action="{{ route('save-user', $store->store_id_pk) }}" @endif  method="post">
            {{ csrf_field() }}
            @if (isset($store->store_title))
                @method('POST')
            @endif
            @if($errors->any())
                <div class="alert alert-danger" role="alert">
                    {{$errors->first()}}
                </div>
            @endif
            <div class="row">
                <div class="col">
                    <label for="store_title">First Name</label>
                    <input type="text" id="first_name" class="form-control" value="{{ $user->first_name }}" placeholder="First Name" name="first_name">
                </div>
                <div class="col">
                    <label for="store_title">Last Name</label>
                    <input type="text" id="last_name" class="form-control" value="{{ $user->last_name }}" placeholder="Last Name" name="last_name">
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" value="{{ $user->email }}" placeholder="Enter email" name="email">
                </div>
                <div class="col">
                    <label for="role">Role</label>
                    <select name="role" class="form-control" id="role" >
                        @foreach ($roles as $key=>$role)
                            <option value="{{$key}}">{{$role}}</option>
                        @endforeach
                    </select>
{{--                    <label for="working_hours_to">Working Hours To</label>--}}
{{--                    <input type="text" class="form-control timepicker" id="working_hours_to" value="{{ $store->working_hours_to }}" placeholder="Working Hours To" name="working_hours_to">--}}
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <label for="working_hours_to">Password</label>
                    <input type="password" autocomplete="new-password" class="form-control timepicker" id="password" value="" placeholder="Password" name="password">
                </div>
                <div class="col">
                    <label for="room_limit">Participant Limit</label>
                    <input type="text" class="form-control" id="room_limit" value="{{ $user->room_limit }}"
                           placeholder="Default is 49" name="room_limit">
                </div>
            </div>


            <div class="row">

                <div class="col">
                    <label for="room_name">Room Name</label>
                    <select class="form-control" name="room_name" id="room_name">
{{--                        @if(isset($user->current_room))--}}
{{--                            <option selected value="{{$user->widget_id}}|{{ $user->current_room }}">{{ $user->current_room }}</option>--}}
{{--                        @else--}}
{{--                            <option value="-1">Select Room</option>--}}
{{--                        @endif--}}
                        @foreach($rooms as $room)
                            <option @if($room->room_name == $user->current_room) selected @endif value="{{ $room->id }}|{{ $room->room_name }}">{{ $room->room_name }}</option>
                        @endforeach

                        {{--                                @foreach($stores as $store)--}}
                        {{--                                    <option data-storeid="{{ $store->store_id_pk }}" value="{{ $store->store_id_pk }}|{{ $store->store_title }}">{{ $store->store_title }}</option>--}}
                        {{--                                @endforeach--}}
                    </select>
                </div>

                <div class="col">
                    {{--                    <label for="store_id">Store</label>--}}
                    {{--                    <select class="form-control" name="store_id" id="store_id">--}}
                    {{--                        @foreach($stores as $store)--}}
                    {{--                            <option @if($store->store_id_pk == $user->store_id) selected @endif data-storeid="{{ $store->store_id_pk }}" value="{{ $store->store_id_pk }}|{{ $store->store_title }}">{{ $store->store_title }}</option>--}}
                    {{--                        @endforeach--}}
                    {{--                    </select>--}}
                </div>
            </div>




            <div class="row">
                <div class="col"></div>
                <div class="col">
                    <input type="submit" style="background: #2699FB; color: white; border: unset;" class="form-control btn btn-info" placeholder="" value="Save">
                </div>
                <div class="col"></div>
            </div>
        </form>

    </div>

    @push('scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
{{--        <script src="{{ asset('js/jquery.timepicker.js') }}"></script>--}}
    @endpush
@section('footer-scripts')

    <script>
        $(function () {
            default_working_hours_from = $('#working_hours_from').val() ? $('#working_hours_from').val() : '9';

            console.log('default_working_hours_from', default_working_hours_from);
            $('#working_hours_from').timepicker({
                timeFormat: 'H:mm',
                interval: 30,
                minTime: '00',
                maxTime: '23:30',
                defaultTime: default_working_hours_from,
                startTime: '00:00',
                dynamic: false,
                dropdown: true,
                scrollbar: true,
                change: function (time) {

                    $working_hours_from_value = $('#working_hours_from').val();
                    $('#working_hours_to').prop('disabled', false);
                    $date = "2022-01-01 " + $working_hours_from_value;
                    var dt = new Date($date);
                    dt.setMinutes( dt.getMinutes() + 30 );
                    $working_hours_to_value = dt.toLocaleTimeString('en-US');
                    default_working_hours_to = $('#working_hours_to').val() ? $('#working_hours_to').val() : $working_hours_to_value;
                    console.log($working_hours_from_value);
                    if($('#working_hours_to').timepicker())
                        $('#working_hours_to').timepicker().destroy();
                    $('#working_hours_to').timepicker({
                        timeFormat: 'H:mm',
                        interval: 30,
                        minTime: default_working_hours_to,
                        maxTime: '23:30',
                        defaultTime: default_working_hours_to,
                        startTime: default_working_hours_to,
                        dynamic: false,
                        dropdown: true,
                        scrollbar: true

                    });

                }
            });

            // $('#working_hours_to').prop('disabled', true);


            $('#working_hours_from').focusout(function() {


            });


            // $('select#store_id').on('change', function() {
            //     // $store_id = $(this).data('storeid');
            //     $("#room_name").html('');
            //     let $store_id = $(this).find(":selected").data('storeid');
            //     $.ajax({
            //         type: "GET",
            //         url : "/admin/widgets/getRooms/" + $store_id,
            //         success : function (data) {
            //             $.each(data, function(index, item){
            //                 let $option = '<option value="' + item.id + '|' + item.room_name + '">' + item.room_name + '</option>'
            //                 $("#room_name").html($option);
            //             });
            //         }
            //     });
            // });

            // $('select#store_id').trigger('change');


        });
    </script>
@endsection

@endsection
