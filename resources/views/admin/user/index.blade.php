@extends('layouts.app')
{{--{{ dd(get_defined_vars()) }}--}}


@section('header-styles')
    <style>
        body {
            background: #fff;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-md-4" style="float: left">
                <a href="{{ route('create-user', $store_id) }}" style="background: #2699FB; color: white; border: unset;" class="form-control btn btn-info">+ Add Promoter</a>
            </div>

            <div class="col-md-4" style="float: left"></div>
            <div class="col-md-4" style="float: left"></div>

            @if(session()->has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong></strong> {{ session()->get('message') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif

            <div class="col-md-12">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Created At</th>
                        <th scope="col">Store</th>
                        <th scope="col">Room</th>
                        <th scope="col">Limit</th>
                        <th scope="col" style="text-align: center">Actions</th>
                    </tr>

                    </thead>
                    <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <th scope="row">{{ $loop->iteration  }}</th>
                            <td>{{ $user->full_name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->created_at }}</td>
                            <td>{{ $user->store_title }}</td>
                            <td>{{ $user->current_room }}</td>
                            <td>{{ $user->room_limit }}</td>
                            <td style="text-align: center">
                                <a href="{{ route('edit-promoter',$user->id) }}" style="margin: 0 5px">
                                    <i class="fa fa-pen"></i>
                                </a>

                                <a href="{{ route('delete-promoter',$user->id) }}" style="margin: 0 5px">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
