@extends('layouts.app')
{{--{{ dd(get_defined_vars()) }}--}}
@section('header-styles')
    <style>
        .text-center {
            text-align: center
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                {{--            <pre>--}}
                {{--                {{ $stores[0]->scheduleConfig }}--}}
                {{--            </pre>--}}
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Store Name</th>
                            <th scope="col">Working Hours</th>
                            <th scope="col">Email</th>
                            <th scope="col">Timezone</th>
                            <th scope="col">Website</th>
                            <th scope="col" class="text-center">Widgets</th>
                            <th scope="col" class="text-center">Promoters</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                    @foreach ($stores as $store)
                        <tr>
                            <th scope="row">{{ $loop->iteration  }}</th>
                            <td>{{ $store->store_title }}</td>
                            <td>{{ $store->scheduleConfig->schedule_title }}</td>
                            <td>{{ $store->email }}</td>
                            <td>{{ $store->timezone->text }}</td>
                            <td>{{ $store->website }}</td>

                            <td class="text-center">
                                <a href="{{ url('/admin/widgets/' . $store->store_id_pk)}}">
                                    <i class="fas fa-tachometer-alt"></i>
                                </a>
                            </td>

                            <td class="text-center">
                                <a href="{{ url('/admin/users/' . $store->store_id_pk)}}">
                                    <i class="fas fa-user"></i>
                                </a>
                            </td>

                            <td>
                                {{--                            <form method="post" action="{{route('edit-store',$store->store_id_pk)}}" style="display: inline-block">--}}
                                {{--                                @method('PUT')--}}
                                {{--                                @csrf--}}
                                {{--                                <button type="submit" style="border: none; color: #2699fb; margin: 0 5px; background: none" ><i class="fas fa-pen"></i></button>--}}
                                {{--                            </form>--}}
                                <a href="{{ route('edit-store',$store->store_id_pk) }}" style="margin: 0 5px"><i class="fas fa-pen"></i></a>
                                <form method="post" action="{{route('delete-store', $store->store_id_pk)}}" style="display: inline-block">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" style="border: none; color: #2699fb; margin: 0 5px;  background: none"><i class="fas fa-times"></i></button>
                                    {{--                                <button type="button" style="border: none; color: #2699fb" class="btn-close" aria-label="Close"></button>--}}


                                </form>


                                {{--                            <a href="{{ route('delete-store', $store->store_id_pk) }}" style="margin: 0 5px"><i class="fas fa-times"></i></a>--}}
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
