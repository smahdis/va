@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
@endpush
@if(isset($store->store_title))
    @section('pageTitle', 'editing ' . $store->store_title)
@else
    @section('pageTitle', 'Add Store')
@endif
{{--{{ dd(get_defined_vars()) }}--}}
{{--{{$schedule_configs}}--}}


@section('content')
    <div class="container">
        <form enctype="multipart/form-data" @if (isset($store->store_title))action="{{ route('update-store', $store->store_id_pk) }}" method="post" @else (isset($store->store_title))action="{{ route('add-store-submit') }}" method="post" @endif>
            {{ csrf_field() }}
            @if (isset($store->store_title))
                @method('PUT')
            @endif
            <div class="row">
                <div class="col">
                    <label for="store_title">Store Name</label>
                    <input type="text" id="store_title" class="form-control" value="{{ $store->store_title }}" placeholder="Store Name" name="store_title">
                </div>

                <div class="col">
                    <label for="working_hours_from">Schedule</label>
{{--                    <input autocomplete="off" type="text" id="working_hours_from" class="form-control timepicker" value="{{ $store->working_hours_from }}" placeholder="Working Hours From" name="working_hours_from">--}}
{{--                </div>--}}
                    <div style="display: grid; grid-template-columns: 5fr 1fr; grid-gap: 10px; border-radius: 12px;">

                        <select name="schedule_config_id"  class="form-control">
    {{--                        <option value="1">Custom</option>--}}
    {{--                        <option value="1">Disabled</option>--}}
                            @foreach ($schedule_configs as $schedule_config)
                                <option @if($store->schedule_config_id==$schedule_config->schedule_config_id_pk) selected @endif value="{{$schedule_config->schedule_config_id_pk}}">{{$schedule_config->schedule_title}}</option>
                            @endforeach
                        </select>
                        @if ($store->store_id_pk)
                            <button type="button"  onclick="window.location='{{ route('schedule', $store->store_id_pk) }}'" style="background: #2699FB; color: white; border: unset;" class="form-control btn btn-info" >Customize</button>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" value="{{ $store->email }}" placeholder="Enter email" name="email">
                </div>
                <div class="col">
{{--                    <label for="working_hours_to">Working Hours To</label>--}}
{{--                    <input type="text" class="form-control timepicker" id="working_hours_to" value="{{ $store->working_hours_to }}" placeholder="Working Hours To" name="working_hours_to">--}}
                    <label for="website">Website</label>
                    <input type="text" class="form-control" id="website" value="{{ $store->website }}" placeholder="Website" name="website">
                </div>


            </div>

            <div class="row">
                <div class="col">
                    <label for="country">Timezone</label>
                    <select name="timezone_id" class="form-control">
                        <option value="-1">Select Timezone</option>
                        @foreach ($timezones as $tzone)
                            <option @if($store->timezone_id==$tzone->timezone_id_pk) selected @endif value="{{$tzone->timezone_id_pk}}">{{$tzone->text}}</option>
                        @endforeach
                    </select>
{{--                    <label for="country">Country</label>--}}
{{--                    <input type="text" class="form-control" id="country" placeholder="Country" name="country">--}}
                </div>
                <div class="col">
{{--                    <label for="city">City</label>--}}
{{--                    <input type="text" class="form-control" id="city" placeholder="City" name="city">--}}
{{--                    <label for="website">Website</label>--}}
{{--                    <input type="text" class="form-control" id="website" value="{{ $store->website }}" placeholder="Website" name="website">--}}
                    <label for="country">Logo</label>
                    @if($store) <input type="hidden" id="upload_file_changed" name="upload_file_changed" value="0"/> @endif
                    <div>
                        <label class="custom-file-upload" style="background: #2699FB;border-radius: 5px;color: #fff;padding: 10px 20px;cursor: pointer;">
                            <input @if(isset($store->store_logo_url)) value="{{$store->store_logo_url}}" @endif name="store_logo" style="display: none"  accept="image/*" type="file"/>
                            Custom Upload
                        </label>

                        <img style="width: 150px" id="logo_preview" src="/file/{{$store->store_logo_url}}">
                    </div>

                </div>
            </div>


            <div class="row">
                <div class="col"></div>
                <div class="col">
                    <input type="submit" style="background: #2699FB; color: white; border: unset;" class="form-control btn btn-info" placeholder="" value="Save">
                </div>
                <div class="col"></div>
            </div>
        </form>

    </div>

    @push('scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
{{--        <script src="{{ asset('js/jquery.timepicker.js') }}"></script>--}}
    @endpush
@section('footer-scripts')

    <script>
        $(function () {

            $('.custom-file-upload').on('change',function(e){
                console.log("test");
                loadFile(e);
            });

            function loadFile(event) {
                console.log(event);
                $("#upload_file_changed").val(1);
                var output = document.getElementById('logo_preview');
                output.src = URL.createObjectURL(event.target.files[0]);
                output.onload = function() {
                    URL.revokeObjectURL(output.src) // free memory
                }
            };

            default_working_hours_from = $('#working_hours_from').val() ? $('#working_hours_from').val() : '9';

            console.log('default_working_hours_from', default_working_hours_from);
            $('#working_hours_from').timepicker({
                timeFormat: 'H:mm',
                interval: 30,
                minTime: '00',
                maxTime: '23:30',
                defaultTime: default_working_hours_from,
                startTime: '00:00',
                dynamic: false,
                dropdown: true,
                scrollbar: true,
                change: function (time) {

                    $working_hours_from_value = $('#working_hours_from').val();
                    $('#working_hours_to').prop('disabled', false);
                    $date = "2022-01-01 " + $working_hours_from_value;
                    var dt = new Date($date);
                    dt.setMinutes( dt.getMinutes() + 30 );
                    $working_hours_to_value = dt.toLocaleTimeString('en-US');
                    default_working_hours_to = $('#working_hours_to').val() ? $('#working_hours_to').val() : $working_hours_to_value;
                    console.log($working_hours_from_value);
                    if($('#working_hours_to').timepicker())
                        $('#working_hours_to').timepicker().destroy();
                    $('#working_hours_to').timepicker({
                        timeFormat: 'H:mm',
                        interval: 30,
                        minTime: default_working_hours_to,
                        maxTime: '23:30',
                        defaultTime: default_working_hours_to,
                        startTime: default_working_hours_to,
                        dynamic: false,
                        dropdown: true,
                        scrollbar: true

                    });

                }
            });

            // $('#working_hours_to').prop('disabled', true);


            $('#working_hours_from').focusout(function() {


            });

        });
    </script>
@endsection

@endsection
