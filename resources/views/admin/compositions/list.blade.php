@extends('layouts.app')
@section('header-styles')
    <style>
        .text-center {
            text-align: center
        }
    </style>
@endsection

{{--{{ json_encode($compositions,TRUE)}}--}}
{{--{{$compositions}}--}}
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">SID</th>
                        <th scope="col">Room SID</th>
                        <th scope="col">Date</th>
                        <th scope="col">Duration</th>
                        <th scope="col">Format</th>
                        <th scope="col" class="text-center">Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($compositions as $composite)
                            <tr>
                                <td>{{ $loop->index }}</td>
                                <td>
                                    <a href="{{ route("downloadComposition", $composite->sid) }}">{{$composite->sid}}</a>
                                </td>
                                <td>{{$composite->roomSid}}</td>
                                <td>{{ $composite->dateCreated->format('Y-m-d H:i:s'); }}</td>
                                <td>{{$composite->duration}} sec</td>
                                <td>{{$composite->format}}</td>
                                <td>
                                    <a href="#">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                    </a>

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
