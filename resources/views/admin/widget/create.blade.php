@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/spectrum-colorpicker2/dist/spectrum.min.css">
    <style>
        label.form-check-label {
            margin-top: 15px;
        }
        input[type=radio] {
            margin: auto !important;
            display: block;
            float: unset !important;
            transform: scale(2);
        }

        .form-check-inline {
            text-align: center;
        }

        .mt-20 {
            margin-top: 20px;
        }

        .radio-grid {
            display: grid;
            grid-template-columns: 1fr 1fr 1fr;
        }

        .container {
            padding: 0 40px;
        }
    </style>
@endpush
@if(isset($store->store_id))
    @section('pageTitle', 'editing ' . $store->store_title)
@else
    @section('pageTitle', 'Add Widget')
@endif

{{--{{$widget}}--}}


@section('content')
    <div class="container">
        <form enctype="multipart/form-data"
              @if (!isset($widget->widget_type))
                action="{{ route('save-widget', $store->store_id_pk) }}" method="post"
              @else (isset($store->store_title))
                action="{{ route('update-widget', [$widget->id, $widget->store_id]) }}" method="post"
              @endif>

            {{ csrf_field() }}
{{--            @if (isset($widget->widget_type))--}}
{{--                @method('PUT')--}}
{{--            @else--}}
                @method('POST')
{{--            @endif--}}

            <div class="row">
                <div class="col-6">
                    <div class="row">
                        <label style="display: block;margin-top: 30px">Choose Widget Type</label>
                        <div class="col-12 mt-20 radio-grid">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" @if($widget->widget_type=='small_banner') checked="checked" @endif type="radio" name="widget_type" id="widget_type1" disabled value="1">
                                <label class="form-check-label" for="inlineRadio1">Small Banner</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" @if($widget->widget_type=='big_banner') checked="checked" @endif type="radio" name="widget_type" id="widget_type2" disabled value="2">
                                <label class="form-check-label" for="inlineRadio2">Big Banner</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" @if($widget->widget_type=='small_logo') checked="checked" @endif @if(!$widget) checked @endif type="radio" name="widget_type" id="widget_type3" value="3">
                                <label class="form-check-label" for="inlineRadio3">Small Logo</label>
                            </div>
                        </div>

                        <label style="display: block;margin-top: 30px">Vertical Positioning</label>
                        <div class="col-12 mt-20 radio-grid">

                            <div class="form-check form-check-inline">
                                <input class="form-check-input" @if($widget->vertical_position=='bottom') checked @endif type="radio" name="vertical_position" id="vertical_position1" @if(!$widget) checked @endif value="1">
                                <label class="form-check-label" for="inlineRadio1">Bottom</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" @if($widget->vertical_position=='middle') checked @endif type="radio" name="vertical_position" id="vertical_position2"  value="2">
                                <label class="form-check-label" for="inlineRadio2">Middle</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" @if($widget->vertical_position=='top') checked @endif type="radio" name="vertical_position" id="vertical_position3"  value="3">
                                <label class="form-check-label" for="inlineRadio3">Top</label>
                            </div>
                        </div>

                        <label style="display: block;margin-top: 30px">Horizontal Positioning</label>
                        <div class="col-12 mt-20 radio-grid">

                            <div class="form-check form-check-inline">
                                <input class="form-check-input" @if($widget->horizontal_position=='left') checked @endif type="radio" name="horizontal_position" id="horizontal_position1" @if(!$widget) checked @endif value="1">
                                <label class="form-check-label" for="inlineRadio1">Left</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" @if($widget->horizontal_position=='right') checked @endif type="radio" name="horizontal_position" id="horizontal_position2" value="2">
                                <label class="form-check-label" for="inlineRadio2">Right</label>
                            </div>

                        </div>



                    </div>
                </div>


                <div class="col-6">
                    <div class="row">
                        <div style="box-shadow: 1px 2px 5px #888888; padding: 2rem; border-radius: 10px;">
                            <div style="padding: 4rem; border: 1px dashed #c7c6c6; display: flex; align-items: center; justify-content: center; flex-direction: column;border-radius: 10px;">

                                @if($widget)<img style="width: 150px" id="chatButtonIconPreview" src="{{$widget->logo_url}}"> @else <img id="chatButtonIconPreview" src="/img/upload_logo.png"> @endif
{{--                                <h3 style="font-weight: bold">Drag and drop files here</h3>--}}
{{--                                <div style="margin-bottom: 10px;">or</div>--}}
                                    @if($widget) <input type="hidden" id="upload_file_changed" name="upload_file_changed" value="0"/> @endif
                                <div>
                                    <label class="custom-file-upload" style="background: #2699FB;border-radius: 5px;color: #fff;padding: 10px 20px;cursor: pointer;">
                                        <input @if(isset($widget->logo_url)) value="{{$widget->logo_url}}" @endif name="logo" style="display: none"  accept="image/*" type="file"/>
                                        Custom Upload
                                    </label>
                                </div>
                            </div>
                        </div>



{{--                        <div style="margin-top: 20px">--}}
{{--                            <div><button class="form-control btn btn-primary">Generate Code</button></div>--}}
{{--                            <div style="margin-top: 20px">--}}
{{--                                <textarea class="form-control" name="generated_code" id="exampleFormControlTextarea1" rows="3">(function(){var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];s1.async=true;s1.src='https://video.demos.ge/config/2';s1.charset='UTF-8';s1.setAttribute('crossorigin','*');s0.parentNode.insertBefore(s1,s0);})();</textarea>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    </div>
                </div>

                <div class="col-12">
                    <div class="row">

                        <div class="col-6">
                            <label for="widget_color">Widget Color</label>
                            <input @if($widget->widget_color) value='{{$widget->widget_color}}' @else value='#009223' @endif  type="text" class="form-control widget_color" id="widget_color" placeholder="Choose Widget Color" name="widget_color">
                        </div>

                        <div class="col-6">
                            <label for="widget_color">Room Name (optional)</label>
                            <input value='{{$widget->room_name}}' type="text" class="form-control room_name" id="room_name" placeholder="Leave empty to generate one automatically" name="room_name">
                        </div>
                    </div>
                </div>

                <div class="col-12">
                    <div class="row">
                        <label for="country">Waiting Image</label>
                        @if($widget) <input type="hidden" id="upload_file_2_changed" name="upload_file_2_changed" value="0"/> @endif
                        <div>
                            <label class="custom-file-2-upload" style="background: #2699FB;border-radius: 5px;color: #fff;padding: 10px 20px;cursor: pointer;">
                                <input @if(isset($widget->waiting_image)) value="{{$widget->waiting_image}}" @endif name="waiting_image" style="display: none"  accept="image/*" type="file"/>
                                Custom Upload
                            </label>

                            <img style="width: 150px" id="waiting_image_preview" src="/file/{{$widget->waiting_image}}">
                        </div>

                    </div>
                </div>

            </div>





            <div class="row">
                <div class="col"></div>
                <div class="col">
                    <input type="submit" style="background: #2699FB; color: white; border: unset;" class="form-control btn btn-info" placeholder="" value="Save">
                </div>
                <div class="col"></div>
            </div>

        </form>

    </div>

    @push('scripts')
        <script src="https://cdn.jsdelivr.net/npm/spectrum-colorpicker2/dist/spectrum.min.js"/>
        <script src="/js/droppable.js"/>

        <script>




            $(function () {

            });
        </script>

    @endpush

@endsection
@section('footer-scripts')

    <script>
        // custom-file-upload
        $('.custom-file-upload').on('change',function(e){
            console.log("test");
            loadFile(e);
        });

        function loadFile(event) {
            console.log(event);
            $("#upload_file_changed").val(1);
            var output = document.getElementById('chatButtonIconPreview');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src) // free memory
            }
        };


        $('.custom-file-2-upload').on('change',function(e){
            console.log("test");
            loadFile2(e);
        });

        function loadFile2(event) {
            console.log(event);
            $("#upload_file_2_changed").val(1);
            var output = document.getElementById('waiting_image_preview');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src) // free memory
            }
        };

        setTimeout(function() {
            $("#widget_color").spectrum({
                type: "component"
            });
        }, 1000);

    </script>
@endsection


