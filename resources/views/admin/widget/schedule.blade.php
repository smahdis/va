@extends('layouts.app')
{{--{{ dd(get_defined_vars()) }}--}}
@push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/spectrum-colorpicker2/dist/spectrum.min.css">
    <style>
        .ui-widget-header{
            border: 1px solid #dddddd;
            background: #50b648;
        }

        .ui-state-default, .ui-widget-content .ui-state-default {
            border: 3px solid #50b648;
            background: #ffffff;
            font-weight: normal;
            border-radius: 10px;
        }

        .slider-range {
            background: #f7f7f9;
            border: unset !important;
            height: 0.3em !important;
        }

        .ui-slider-horizontal .ui-slider-handle {
            top: -0.45em;
        }


        .min, .max {
            width: 50px;
            text-align: center;
            font-size: 10px;
        }

        .form-check-input:checked {
            background-color: #50b648;
            border-color: #50b648;
        }

        .day-cart {
            box-shadow: 0px 3px 3px #88888859;
            padding: 15px 30px 20px 30px;
            border-radius: 5px;
            margin-top: 2rem;
            border: 1px solid #dbdbdb;
        }

        .slider-parent:not(:first-child) {
            margin-top: 30px;
        }

        .slider-parent {
            max-height: 25px;
        }

        .add-time {
            font-size: 13px;
            font-family: Lato,Arial,Helvetica,sans-serif;
        }

        .form-check.form-switch {
            margin-top: 25px;
        }

        .fa-times {
            color: #c5c5c5;
        }

        .remove-shift-container {
            text-align: center;
        }

        .add-new-shift {
            margin-top: 20px;
        }
    </style>
@endpush



{{--<div class="col-md-1 add-new-shift" style="margin-left: -10px;"><a href="javascript:void(0)sans;" data-day= class="add-time" >+ Add New</a></div>--}}
@section('content')
    <div class="container">
        <div class="row justify-content-center">

{{--            {{ json_encode($schedules,TRUE)}}--}}

            <form method="POST" action="{{ route('save-schedule', $store_id) }}">
                {{ csrf_field() }}
                @method('POST')

                <div class="body">
                <div class="col-md-12" style="margin-top: 2rem">
{{--                    @foreach ($schedules as $key=>$week_day)--}}
{{--                        <h1>{{$week_day}}</h1>--}}
{{--                    @endforeach--}}
                    @foreach ($week_days as $key=>$week_day)
                        @php $open = 0 @endphp
                        <div class="col-md-12 day-cart">
                            <p>
                                <label for="amount">{{ $week_day->day_title }}:</label>
                                <input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
                            </p>
{{--                            {{ json_encode($schedules,TRUE)}}--}}
                            <div class="row justify-content-center">
                                <div class="col-md-1">
                                    <div class="form-check form-switch">
                                        @if(!empty($schedules))
                                            @foreach ($schedules as $key1=>$schedule)
                                                @if($key1 == $week_day->day_id_pk)
                                                    <input class="form-check-input toggle-store-close" type="checkbox" @if($schedule[0]->status) @php $open = 1 @endphp checked @endif name="schedule[{{ $week_day->day_id_pk }}][0][status]">
                                                    @break
                                                @endif
                                            @endforeach
                                        @else
                                            @php $open = 1 @endphp
                                            <input class="form-check-input toggle-store-close" type="checkbox" checked name="schedule[{{ $week_day->day_id_pk }}][0][status]">
                                        @endif
                                    </div>
                                </div>
                                <div @if(!$open) style="display: none" @endif class="col-md-10 shifts-container">
                                    @if(!empty($schedules))
                                        @foreach ($schedules as $key1=>$schedule)
                                            @if($key1 == $week_day->day_id_pk)
                                                @foreach ($schedule as $key2=>$shift)
                                                    <div class="row">
                                                        <div class="slider-parent col-sm-11">
                                                            <div id="slider-range{{$key2}}" class="slider-range" style="margin-top: 8px;" data-from_time="{{$shift->from_time}}" data-to_time="{{$shift->to_time}}"></div>
                                                            <div id="min{{$key2}}" class="min" data-from_time="{{$shift->from_time}}"></div>
                                                            <div id="max{{$key2}}" class="max" data-to_time="{{$shift->to_time}}"></div>
                                                            <input type="hidden" class="start" name="schedule[{{ $week_day->day_id_pk }}][{{$key2}}][start]" value="{{$shift->from_time}}"/>
                                                            <input type="hidden" class="end" name="schedule[{{ $week_day->day_id_pk }}][{{$key2}}][end]" value="{{$shift->to_time}}"/>
                                                        </div>
                                                        <div class="col-sm-1 remove-shift-container">
                                                            @if (!$loop->first)
                                                                <a href="#" class="remove-shift"><i class="fas fa-times"></i></a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @else

                                        <div class="row">
                                            <div class="slider-parent col-sm-11">
                                                <div id="slider-range1" class="slider-range" style="margin-top: 8px;" data-from_time="0" data-to_time="1440"></div>
                                                <div id="min1" class="min" data-from_time="0"></div>
                                                <div id="max1" class="max" data-to_time="1440"></div>
                                                <input type="hidden" class="start" name="schedule[{{ $week_day->day_id_pk }}][0][start]" value="0"/>
                                                <input type="hidden" class="end" name="schedule[{{ $week_day->day_id_pk }}][0][end]" value="1440"/>
                                            </div>
                                            <div class="col-sm-1 remove-shift-container"></div>
                                        </div>

                                    @endif
                                    <div class="col-md-1 add-new-shift" style="margin-left: -10px;"><a href="javascript:void(0);" data-day="{{ $week_day->day_id_pk }}" class="add-time" >+ Add New</a></div>
                                </div>

                                <div @if($open) style="display: none" @endif class="col-md-10 close-container">
                                    <div class="row">
                                        <label>Closed</label>
                                    </div>
                                </div>

                            </div>
                        </div>
                    @endforeach

                </div>

                <footer class="col-md-12" style="margin: 2rem">
{{--                    <button class="btn btn-default ">Cancel</button>--}}
                    <button type="submit" class="btn btn-primary">Save</button>
                </footer>

            </div>
            </form>
        </div>
    </div>


@endsection



@section('footer-scripts')
    <script>
        $( function() {
            $(document).on("change",".toggle-store-close",function(e) {
                $(this).parents('.day-cart').find('.shifts-container').toggle();
                $(this).parents('.day-cart').find('.close-container').toggle();

                $('.min').each(function(i, obj) {
                    // var $of_elem = $('#slider-range' + (i+1) + ' span:eq(0)');
                    var $of_elem = $(this).siblings('.slider-range').find('span:eq(0)');
                    var $from_time = $(obj).data('from_time');
                    leftHandler(obj, $of_elem, $from_time);
                });

                $('.max').each(function(i, obj) {
                    var $of_elem = $(obj).siblings('.slider-range').find('span:eq(1)');
                    // var $of_elem = $('#slider-range' + (i+1) + ' span:eq(1)');
                    var $to_time = $(obj).data('to_time');
                    rightHandler(obj, $of_elem, $to_time);
                });
            });

            $(document).on("click",".remove-shift",function(e) {
                e.preventDefault();
                $(this).closest('.row').fadeOut(300, function() { $(this).remove(); });
            });


            $(document).on("click",".add-time",function() {
                $day = $(this).data('day');
                $number_of_slider_in_this_day = $(this).parent().parent().find('.slider-parent').length;


                $elem = $(
                    '<div class="row">' +
                        '<div class="slider-parent col-sm-11">' +
                            '<div class="slider-range" style="margin-top: 8px;"></div>' +
                            '<div class="min"></div>' +
                            '<div class="max"></div>' +
                            '<input type="hidden" class="start" name="schedule[' + $day + '][' + ($number_of_slider_in_this_day) + '][start]" value="540"/>' +
                            '<input type="hidden" class="end" name="schedule[' + $day + '][' + ($number_of_slider_in_this_day) + '][end]" value="1155"/>' +
                        '</div>' +
                        '<div class="col-sm-1 remove-shift-container">' +
                            '<a href="https://virtual-assistants.gr/admin/stores/add"><i class="fas fa-times"></i></a>' +
                        '</div>' +
                    '</div>');

                $elem.insertBefore(
                    $(this).parent()
                );

                $slider_elem = $elem.find('.slider-range');

                makeSliders($slider_elem, 540, 1140);
                console.log($slider_elem.find('span:eq(0)'));
                leftHandler($slider_elem.siblings('.min'), $slider_elem.find('span:eq(0)'),540);
                rightHandler($slider_elem.siblings('.max'), $slider_elem.find('span:eq(1)'),1140);

            });

            $(".slider-range").each(function(i, obj) {
                console.log($(obj).data('from_time'));
                console.log($(obj).data('to_time'));
                makeSliders(obj, $(obj).data('from_time'), $(obj).data('to_time'));
            });

            function makeSliders($elem, $start=0, $end=1440) {
                // $(".slider-range").slider({
                $($elem).slider({
                    range: true,
                    min: 0,
                    max: 1440,
                    step: 15,
                    values: [ $start, $end ],
                    slide: function(event, ui) {

                        $min = $(this).siblings('.min');
                        $max = $(this).siblings('.max');

                        var delay = function() {
                            //var handleIndex = $(ui.handle).data('index.uiSliderHandle');

                            console.log(ui.value);

                            var handleIndex = $(ui.handle).index();
                            var label = handleIndex == 1 ? $min : $max;

                            // var hours = Math.floor(ui.value / 60);
                            // var minutes = ui.value - (hours * 60);
                            //
                            // if(hours.toString().length == 1) hours = '0' + hours;
                            // if(minutes.toString().length == 1) minutes = '0' + minutes;
                            $time = min_to_time(ui.value);

                            // $(label).html(ui.value).position({
                            label.html($time).position({
                                my: 'center bottom',
                                at: 'center top',
                                of: ui.handle,
                                offset: "0, 10"
                            });
                        };

                        // wait for the ui.handle to set its position
                        setTimeout(delay, 5);
                    },
                    stop: function (event, ui) {
                        $(this).siblings('input.start').val(ui.values[0]);
                        $(this).siblings('input.end').val(ui.values[1]);
                    }

                });
            }


            $('.min').each(function(i, obj) {
                // var $of_elem = $('#slider-range' + (i+1) + ' span:eq(0)');
                var $of_elem = $(this).siblings('.slider-range').find('span:eq(0)');
                var $from_time = $(obj).data('from_time');
                leftHandler(obj, $of_elem, $from_time);
            });

            function leftHandler($elem, $of_elem, $time=0) {
                $time = min_to_time($time);

                $($elem).html($time).position({
                    my: 'center bottom',
                    at: 'center top',
                    of: $of_elem,
                    offset: "0, 10"
                });
            }

            $('.max').each(function(i, obj) {
                var $of_elem = $(obj).siblings('.slider-range').find('span:eq(1)');
                // var $of_elem = $('#slider-range' + (i+1) + ' span:eq(1)');
                var $to_time = $(obj).data('to_time');
                rightHandler(obj, $of_elem, $to_time);
            });

            function rightHandler($elem, $of_elem, $time=1440) {
                $time = min_to_time($time);
                $($elem).html($time).position({
                    my: 'center bottom',
                    at: 'center top',
                    of: $of_elem,
                    offset: "0, 10"
                });
            }

            function min_to_time($min)
            {
                var hours = Math.floor($min / 60);
                var minutes = $min - (hours * 60);

                if(hours.toString().length == 1) hours = '0' + hours;
                if(minutes.toString().length == 1) minutes = '0' + minutes;

                return hours + ':' + minutes;

            }
            // $('.max').html('24:00').position({
            //     my: 'center bottom',
            //     at: 'center top',
            //     of: $(this).siblings('.slider-range').find('span:eq(1)'),//$('.slider-range span:eq(1)'),
            //     offset: "0, 10"
            // });

        } );
    </script>
@endsection
