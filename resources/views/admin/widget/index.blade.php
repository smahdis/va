@extends('layouts.app')
{{--{{ dd(get_defined_vars()) }}--}}

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4" style="float: left">
            <a href="{{ route('create-widget', $store_id) }}" style="background: #2699FB; color: white; border: unset;" class="form-control btn btn-info">+ Add Widget</a>
        </div>
        <div class="col-md-4" style="float: left"></div>
        <div class="col-md-4" style="float: left"></div>
        <div class="col-md-12" style="margin-top: 2rem">
            <table class="table table-striped">
                <caption>

                </caption>
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Widget Type</th>
                        <th scope="col">Widget Color</th>
                        <th scope="col">Vertical Position</th>
                        <th scope="col">Horizontal Position</th>
                        <th scope="col">Logo</th>
                        <th scope="col">Generated Code</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($widgets as $widget)
                    <tr>
                        <th scope="row">{{ $loop->iteration  }}</th>
                        <td>{{ $widget->widget_type }}</td>
                        <td>{{ $widget->widget_color }}</td>
                        <td>{{ $widget->vertical_position }}</td>
                        <td>{{ $widget->horizontal_position }}</td>
                        <td>
                            <a href="{{ $widget->logo_url }}">
                                <img style="width: 100px;" src="{{ $widget->logo_url }}"/>
                            </a>
                        </td>
{{--                        <td><input value="{{ $widget->generated_code }}"/></td>--}}
                        <td>
                            <input value="{{ $widget->generated_code }}" class="form-control" style="width: 200px;">
                        </td>
                        <td>
                            <a href="{{ route('edit-widget',$widget->id) }}" style="margin: 0 5px"><i class="fas fa-pen"></i></a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
