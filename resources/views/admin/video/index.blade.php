@extends('layouts.app')
@section('header-styles')
    <style>
        #video-container {
            display: grid;
            /*grid-template-columns: repeat(auto-fit, minmax(100px, 1fr));*/
            grid-template-columns: repeat(6, 1fr);
            grid-gap: 1.5rem;
            /*justify-items: center;*/
            margin: 0;
            padding: 0;

        }

        .participant-admin {
            grid-column: 1 / 4;
            grid-row: 1/3;
        }
        #video-container video {
            width: 100%
        }

        .control-buttons {
            padding: 5px 0px 5px 0;background: aqua;
        }

        .mic-mute {
            border-radius: 0px 0px 0px 10px
        }
        .vid-mute {
            border-radius: 0px 0px 10px 0px
        }

        #local-media {
            display: grid;
            justify-items: center;
            align-items: center;
        }

        #local-media video{
            width: 50%;
            grid-column: 1;
            grid-row: 1 / 4;
        }


    </style>
@endsection
@section('content')
{{--    {{ json_encode($rooms,TRUE)}}--}}
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                @if(session()->has('message'))
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong></strong> {{ session()->get('message') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @else
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        @if ($current_room)
                            You are currently promoting <strong>{{ $current_room }}</strong>. <a href="{{ route('leaveRoom', $current_room) }}">Click here to exit</a>
                        @else
                            You are currently not in any room
                        @endif
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif


                <div style="display: grid;grid-template-columns: 1fr 1fr 1fr 1fr;">
                    @forelse($rooms as $room)
                        <a style="text-decoration: none; color: #fff" href="{{ route("preview", ["widget_id" => $room->id, "room_name" => $room->room_name]) }}">
                            <div style="background: #343334;padding: 50px;text-align: center; border-radius: 10px;">
                                {{$room->room_name}}
                            </div>
                        </a>
                    @empty
                        <div class="alert alert-danger alert-dismissible fade show" role="alert" style="grid-column: 4 span;">
                            No rooms found
{{--                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>--}}
                        </div>
                    @endforelse
                </div>


            </div>
        </div>
    </div>

@endsection


@section('footer-scripts')
    <script>

    </script>
@endsection
