@extends('layouts.app')
@section('header-styles')
    <style>
        body {
            background-color: rgb(32, 33, 36)
        }

        #video-container {
            /*display: grid;*/
            /*!*grid-template-columns: repeat(auto-fit, minmax(100px, 1fr));*!*/
            /*grid-template-columns: repeat(6, 1fr);*/
            /*grid-gap: 1.5rem;*/
            /*!*justify-items: center;*!*/
            /*margin: 0;*/
            /*padding: 0;*/
            margin: 0 auto;
            width: 100%;
        }
        .participant video {
            width: 100%;
            border-radius: 10px;
            max-height: 210px;
        }

        .participant-admin video {
            width: 100%;
            border-radius: 10px;
        }

        .participant-admin {
            grid-column: 1 / 6;
            grid-row: 1 / 5;
            border-radius: 10px;
            width: 60%;
            margin: 0 auto;
        }


        .participant {
            /* grid-column: 6; */
            border-radius: 4px;
        }

        #video-container video {
            width: 100%
        }

        .control-buttons {
            padding: 5px 0px 5px 0;background: aqua;
        }

        .mic-mute {
            border-radius: 0px 0px 0px 10px
        }
        .kick {
            border-radius: 0px 0px 10px 0px
        }

        .participants-other{
            display: grid;
            grid-template-columns: repeat(3, 1fr);
            margin-top: 10px;
            gap: 10px;
        }
        .participants-other .participant img.mutevid {
            max-height: 210px;
        }


    </style>
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    You are currently promoting in {{ $room_name }}
                    <a href="{{ route('leaveRoom', ["room_name" => $room_name, "room_name_base64" => $token_info['roomName']]) }}">Click here to exit</a>
                </div>

                <div id="video-container">
                    <div class="participants-other"></div>
                </div>
                <div style="display: flex;justify-content: center;align-items: center;">
                    {{--                    {{ json_encode($token_info,TRUE) }}--}}
{{--                    <input type="hidden" id="fullname" value="{{ Auth::user()->name}}">--}}
{{--                    <button type="button" id="connectButton" class="btn btn-primary">Connect</button>--}}
                </div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')
    <script src="https://sdk.twilio.com/js/video/releases/2.20.1/twilio-video.min.js"></script>
    <script src="/js/twilio-video-processors.js"></script>
    <script src="{{ asset('js/videochat.js?v=124') }}" defer></script>
@endpush


@section('footer-scripts')
    <script>
        var token;
        var roomName;
        document.addEventListener("DOMContentLoaded", function(event) {
            token = "{{ $token_info["token"] }}";
            roomName = "{{ $token_info["roomName"] }}";
            startRoom();
        });

        let $video_background_type = null;
        @if(Auth::user()->video_background_type)
            $video_background_type = {{Auth::user()->video_background_type}};
        @endif

        function handleBackgroundProcessing(track, $element) {

            if($video_background_type == 2) {
                const showBlurAsBackground = async () => {
                    // console.log("blur preview");
                    // const videoTrack = await Twilio.Video.createLocalVideoTrack({
                    //     width: 640,
                    //     height: 480,
                    //     frameRate: 24
                    // });
                    // document.getElementById('local-media').querySelector(".preview").innerHTML = "";
                    // document.getElementById('local-media').querySelector(".preview").appendChild(videoTrack.attach());

                    const bg = new Twilio.VideoProcessors.GaussianBlurBackgroundProcessor({
                        assetsPath: '/',
                        maskBlurRadius: 10,
                        blurFilterRadius: 50,
                    });
                    await bg.loadModel();
                    track.addProcessor(bg);
                }

            showBlurAsBackground(track);
            }

            if($video_background_type == 3) {
                // showBlurAsBackground();
                const showImageAsBackground = async () => {
                    // console.log("image preview");
                    // const videoTrack = await Twilio.Video.createLocalVideoTrack({
                    //     width: 640,
                    //     height: 480,
                    //     frameRate: 24
                    // });
                    //
                    // document.getElementById('local-media').querySelector(".preview").innerHTML = "";
                    // document.getElementById('local-media').querySelector(".preview").appendChild(videoTrack.attach());
                    //
                    let img = new Image();
                    // $src = $("#radio3").data("imgsrc");
                    //
                    // console.log('setting image as background: ', '/file/' + $src);
                    img.src = '/file/' + "{{Auth::user()->video_background_image}}"; //'/img/sampleback.jpg';
                    img.onload = async () => {
                        const bg = new Twilio.VideoProcessors.VirtualBackgroundProcessor({
                            assetsPath: '/',
                            backgroundImage: img,
                            maskBlurRadius: 5,
                        });
                        await bg.loadModel();

                        track.addProcessor(bg);
                    }
                }

                showImageAsBackground();
            }
        }

        $(function() {
            $(document).on("click",".control-buttons.mic-mute",function(e) {
                e.preventDefault();
                $uid = $(this).data('uid');
                dataTrack.send(JSON.stringify({
                    "user": 1,
                    "type": "audio",
                    "uid": $uid
                }));
                // alert("hello");
            });
            $(document).on("click",".control-buttons.vid-mute",function(e) {
                e.preventDefault();
                $uid = $(this).data('uid');
                dataTrack.send(JSON.stringify({
                    "user": 1,
                    "type": "video",
                    "uid": $uid
                }));
                // alert("hello");
            });
            $(document).on("click",".control-buttons.kick",function(e) {
                e.preventDefault();
                $uid = $(this).data('uid');
                dataTrack.send(JSON.stringify({
                    "user": 1,
                    "type": "kick",
                    "uid": $uid
                }));
                // alert("hello");
            });

        });
    </script>
@endsection
