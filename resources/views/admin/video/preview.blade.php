@extends('layouts.app')
@section('header-styles')
    <style>
        body {
            background-color: rgb(32, 33, 36)
        }

        #video-container {
            display: grid;
            /*grid-template-columns: repeat(auto-fit, minmax(100px, 1fr));*/
            grid-template-columns: repeat(6, 1fr);
            grid-gap: 1.5rem;
            /*justify-items: center;*/
            margin: 0;
            padding: 0;

        }

        .participant-admin {
            grid-column: 1 / 4;
            grid-row: 1/3;
        }

        #video-container video {
            width: 100%
        }

        .control-buttons {
            padding: 5px 0px 5px 0;
            background: aqua;
        }

        .mic-mute {
            border-radius: 0px 0px 0px 10px
        }

        .vid-mute {
            border-radius: 0px 0px 10px 0px
        }

        #local-media {
            /*display: grid;*/
            /*justify-items: center;*/
            /*align-items: center;*/
            text-align: center;
        }

        #local-media video {
            width: 500px;
            margin: 0 auto;
            display: block;

            /*grid-column: 1;*/
            /*grid-row: 1 / 4;*/
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if($is_promoting)
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        You are currently promoting in {{ $room_name }}
                        <a href="{{ route('leaveRoom', ["room_name" => $room_name, "room_name_base64" => Auth::user()->room_name_base64]) }}">Click
                            here to exit</a>
                    </div>
                @else
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        You are currently not promoting. Enter the room to start.
                    </div>
                @endif
                <div id="local-media" style="text-align: center">
                    <div class="preview">

                    </div>
                    <a style="display: block; width: 500px; margin: 0 auto; padding: 25px 59px;"
                       href="{{ route('connect', ["widget_id" => $widget_id, "room_name" => $room_name]) }}"
                       type="button" id="connectButton" class="btn btn-primary">Σύνδεση</a>
                </div>
            </div>
        </div>





    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Background Settings</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">

                    <div class="row justify-content-center" style="margin: 20px 10px;">
                        <div class="col-12">
                            <form method="post" enctype="multipart/form-data" id="selectVideoType">
                                @method('POST')
                                @csrf
                                <div class="row" style="background: #fff; margin-top: 0;">

                                    <div class="form-check">
                                        <input @if(Auth::user()->video_background_type==1) checked
                                               @endif @if(!Auth::user()->video_background_type) checked @endif type="radio"
                                               class="form-check-input" id="radio1" name="video_background_type" value="1">
                                        <label class="form-check-label" for="check1">No Background</label>
                                    </div>
                                    <div class="form-check">
                                        <input @if(Auth::user()->video_background_type==2) checked @endif type="radio"
                                               class="form-check-input" id="radio2" name="video_background_type" value="2">
                                        <label class="form-check-label" for="check2">Blur</label>
                                    </div>
                                    <div class="form-check">
                                        <input data-imgsrc="{{Auth::user()->video_background_image}}"
                                               @if(Auth::user()->video_background_type==3) checked @endif type="radio"
                                               class="form-check-input" id="radio3" name="video_background_type" value="3">
                                        <label class="form-check-label" for="check2">Custom Image</label>
                                    </div>
                                    <div class="mb-3" style="display: none;">
                                        <label for="video_background_image" class="form-label">
                                            @if(Auth::user()->video_background_image)
                                                <img src="/file/{{Auth::user()->video_background_image}}"
                                                     id="background_image_element" style="cursor: pointer; max-width:200px"/>
                                            @else
                                                Select your background image
                                            @endif
                                        </label>
                                        <input @if(Auth::user()->video_background_image) style="display: none"
                                               @endif class="form-control form-control-sm" id="video_background_image"
                                               value="{{Auth::user()->video_background_image}}" type="file"
                                               name="video_background_image">
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
{{--                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>--}}
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center" style="margin-top: 30px;">
        <div class="col-2" style="text-align: center;background: #a7a7a7;padding: 10px;border-radius: 10px;">
            <i data-toggle="tooltip" data-placement="top" title="Background Settings" class="fas fa-cog" style="color: #fff; cursor: pointer" data-bs-toggle="modal" data-bs-target="#exampleModal"></i>

            <a data-toggle="tooltip" data-placement="top" title="Recordings" href="/admin/promoters/recordings">
                <i class="fas fa-rectangle-list" style="color: #fff; cursor: pointer"></i>
            </a>
        </div>
    </div>


@endsection

@push('scripts')
    <script src="https://sdk.twilio.com/js/video/releases/2.15.2/twilio-video.min.js"></script>
    <script src="/js/twilio-video-processors.js"></script>
@endpush
@section('footer-scripts')
    <script>

        $(function () {
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
            {{--            @if(Auth::user()->video_background_type==1 || !Auth::user()->video_background_type)--}}
            {{--                setVideoBackgroundImage(1);--}}
            {{--            @elseif(Auth::user()->video_background_type==2)--}}
            {{--                setVideoBackgroundImage(2);--}}
            {{--            @elseif(Auth::user()->video_background_type==3)--}}
            {{--                setVideoBackgroundImage(3);--}}
            {{--            @endif--}}


            $(document).on('change', '#video_background_image', function () {
                updatePromoterVideoBackgroundImage(3);
            });

            $(document).on('change', 'input[type=radio][name=video_background_type]', function (event) {
                if ($(this).val() == 3) {
                    $("#video_background_image").parent().css("display", "");
                    $("#video_background_image").prop('disabled', false);
                } else {
                    $("#video_background_image").parent().css("display", "none");
                    $("#video_background_image").prop('disabled', true);
                }

                if ($(this).is(':checked')) {
                    // setVideoBackgroundImage($(this).val());
                    updatePromoterVideoBackgroundImage($(this).val());
                }
            });
        });

        setTimeout(function () {
            $("input[type=radio][name=video_background_type]").trigger('change');
        }, 500);


        function setVideoBackgroundImage($VideoType) {
            console.log($VideoType);
            switch ($VideoType) {
                case '1':
                    // console.log("simple preview");
                    showPreview();
                    break;
                case '2':
                    // console.log("blur preview");
                    showBlurAsBackground();
                    break;
                default:

                    showImageAsBackground();

                    break;
            }


        }

        function updatePromoterVideoBackgroundImage($VideoType) {
            var formData = new FormData(document.getElementById("selectVideoType"));


            $.ajax({
                type: "POST",
                traditional: true,
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                {{--data: {--}}
                    {{--    "_token": "{{ csrf_token() }}",--}}
                    {{--    "video_background_type" : $VideoType--}}
                    {{--},--}}
                url: "/admin/promoters/update-background",
                success: function (data) {
                    if (data) {
                        $("#radio3").data("imgsrc", data);
                        console.log("imgsrc", $("#radio3").data("imgsrc"));
                        console.log("server response", data);
                        // console.log();
                        $("#background_image_element").attr("src", '/file/' + data);
                    }
                    // if($VideoType==)
                    setVideoBackgroundImage($VideoType);
                }
            });
        }


        const showPreview = async (roomName, token) => {
            console.log("simple preview");
            // join the video room with the Access Token and the given room name
            const tracks = await Twilio.Video.createLocalVideoTrack;
            tracks().then(track => {
                const localMediaContainer = document.getElementById('local-media');
                localMediaContainer.querySelector(".preview").innerHTML = "";
                localMediaContainer.querySelector(".preview").appendChild(track.attach());
            });


        };
        // showPreview();


        const showBlurAsBackground = async () => {
            console.log("blur preview");
            const videoTrack = await Twilio.Video.createLocalVideoTrack({
                width: 640,
                height: 480,
                frameRate: 24
            });
            document.getElementById('local-media').querySelector(".preview").innerHTML = "";
            document.getElementById('local-media').querySelector(".preview").appendChild(videoTrack.attach());

            const bg = new Twilio.VideoProcessors.GaussianBlurBackgroundProcessor({
                assetsPath: '/',
                maskBlurRadius: 10,
                blurFilterRadius: 50,
            });
            await bg.loadModel();
            videoTrack.addProcessor(bg);
        }

        // showBlurAsBackground();


        const showImageAsBackground = async () => {
            console.log("image preview");
            const videoTrack = await Twilio.Video.createLocalVideoTrack({
                width: 640,
                height: 480,
                frameRate: 24
            });

            document.getElementById('local-media').querySelector(".preview").innerHTML = "";
            document.getElementById('local-media').querySelector(".preview").appendChild(videoTrack.attach());

            let img = new Image();
            $src = $("#radio3").data("imgsrc");

            console.log('setting image as background: ', '/file/' + $src);
            img.src = '/file/' + $src; //'/img/sampleback.jpg';
            img.onload = async () => {
                const bg = new Twilio.VideoProcessors.VirtualBackgroundProcessor({
                    assetsPath: '/',
                    backgroundImage: img,
                    maskBlurRadius: 5,
                });
                await bg.loadModel();

                videoTrack.addProcessor(bg);
            }
        }

        // showImageAsBackground();

        {{--        var token;--}}
        {{--        var roomName;--}}
        {{--        document.addEventListener("DOMContentLoaded", function(event) {--}}
        {{--            token = "{{ $token_info["token"] }}";--}}
        {{--            roomName = "{{ $token_info["roomName"] }}";--}}
        {{--        });--}}

        {{--        $(function() {--}}
        {{--            $(document).on("click",".control-buttons.mic-mute",function(e) {--}}
        {{--                e.preventDefault();--}}
        {{--                $uid = $(this).data('uid');--}}
        {{--                dataTrack.send(JSON.stringify({--}}
        {{--                    "user": 1,--}}
        {{--                    "type": "audio",--}}
        {{--                    "uid": $uid--}}
        {{--                }));--}}
        {{--                // alert("hello");--}}
        {{--            });--}}
        {{--            $(document).on("click",".control-buttons.vid-mute",function(e) {--}}
        {{--                e.preventDefault();--}}
        {{--                $uid = $(this).data('uid');--}}
        {{--                dataTrack.send(JSON.stringify({--}}
        {{--                    "user": 1,--}}
        {{--                    "type": "video",--}}
        {{--                    "uid": $uid--}}
        {{--                }));--}}
        {{--                // alert("hello");--}}
        {{--            });--}}

        {{--        });--}}
    </script>

@endsection
