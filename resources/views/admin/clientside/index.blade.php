<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <title>Twilio Video Demo</title>
    <!-- Twilio Video CDN -->
    <script src="https://sdk.twilio.com/js/video/releases/2.20.1/twilio-video.min.js"></script>
    <style>
        body, label, h1, h2, h3,h4,h5,h6,a,button {
            font-family: sans-serif;
        }
      .participant video {
        width: 100%;
        border-radius: 10px;
        max-height: 190px;
      }

      .participant-admin video {
        width: 100%;
        border-radius: 10px;
      }

      #video-container {
          width: 50%;
          margin: 0 auto;
      }
      .participant-admin {
        grid-column: 1 / 6;
        grid-row: 1 / 5;
        border-radius: 10px;
      }

      .participant {
        /* grid-column: 6; */
        border-radius: 4px;
      }


      /* Style inputs with type="text", select elements and textareas */
    input[type=text], select, textarea {
      width: 100%; /* Full width */
      padding: 12px; /* Some padding */
      border: 1px solid #ccc; /* Gray border */
      border-radius: 4px; /* Rounded borders */
      box-sizing: border-box; /* Make sure that padding and width stays in place */
      margin-top: 6px; /* Add a top margin */
      margin-bottom: 16px; /* Bottom margin */
      resize: vertical /* Allow the user to vertically resize the textarea (not horizontally) */
    }

    /* Style the submit button with a specific background color etc */
    input[type=submit] {
      background-color: #04AA6D;
      color: white;
      padding: 12px 20px;
      border: none;
      border-radius: 4px;
      cursor: pointer;
    }

    /* When moving the mouse over the submit button, add a darker green color */
    input[type=submit]:hover {
      background-color: #45a049;
    }

    /* Add a background color and some padding around the form */
    .container {
      border-radius: 5px;
      background-color: #2d3035;
      padding: 20px;
        max-width: 500px;
        margin: 0 auto;
    }

      .iframe {
          position: relative;
      }

      .iframe button {
          position: absolute;
          right: 10px;
          top: 10px;
          color: red;
      }

    .participants-other{
        display: grid;
        grid-template-columns: repeat(3, 1fr);
        margin-top: 10px;
        gap: 10px;
    }
    .participants-other .participant img.mutevid {
        max-height: 190px;
    }

    #local-media.preview video{
        max-width: 200px;
    }

    #videoChatContainer {
        display: grid;
        grid-template-columns: 1fr 1fr;
    }

    @media only screen and (max-width: 600px) {
        .participants-other {
            display: unset
        }

        .participant-admin{
            margin-bottom: 20px;
        }

        #videoChatContainer {
            display: grid;
            grid-template-columns: 1fr;
        }

        #local-media {
            margin: 0 auto;
            margin-bottom: 36px;
        }
    }

    </style>
  </head>
  <body>
    <!-- <form id="room-name-form">
      Enter a Room Name to join: <input name="room_name" id="room-name-input" />
      <button type="submit">Join Room</button>
    </form> -->

    <div style="
        display: grid;
        grid-template-columns: repeat(3, 1fr);
        grid-template-rows: repeat(1, 1fr);
        grid-gap: 10px;
        overflow: hidden;" id="grid-container">

        <div style="grid-row: 1;grid-column: 2;align-self: center; text-align: center; justify-self: center; margin: 20px">
            <img src="/file/{{$store->store_logo_url}}" style="max-width: 200px;">
        </div>

        <div style=" grid-column: 2; grid-row: 2;">
            <div class="container1" id="videoChatContainer" style="">
                <div id="local-media" class="preview"><label style="cursor: pointer;color: #b7a1a1;text-align: center;/* width: 50px; */display: block;position: relative;top: 60%;font-size: 12px;">Preview disabled,
                    <br/>click here to show a preview</label></div>
              <form id="videoChatForm">
                <label for="fname" style="color: white">Εισάγετε το όνομα σας</label>
                <input type="text" id="fname" placeholder="Εισάγετε το όνομα σας.." value="">
                <div style="margin-bottom: 10px; color: #fff">
                    <div>
                      <label><input type="checkbox" id="enable_camera" checked>Enable Camera</label>
                    </div>
                    <div>
                      <label><input type="checkbox" id="enable_mic" checked>Enable Microphone</label>
                    </div>
                </div>
                <input type="submit" value="Υποβολή">
              </form>
            </div>
        </div>
    </div>

    <div style="text-align: center;    margin: 15px;">
        <a id="disconnect" href="#" style="display: none; padding: 10px 15px 15px 15px;border-radius: 7px;background: #dc3545;color: white;text-decoration: none;/* border: 3px solid rgb(177 3 3); */font-family: sans-serif;position: fixed;bottom: -10px;right: 46%;">Αποσύνδεση</a>
    </div>

    <div id="video-container">
    <div class="participants-other"></div>
      <!-- <div class="participant-admin">
        <img src="https://riggswealth.com/wp-content/uploads/2016/06/Riggs-Video-Placeholder.jpg"/>
      </div> -->
    </div>
    <script>
        var tracks;
        var localVideoTrack;
        var widget_id;
        var waiting_image_url;

        var url = new URL(window.location);
        window.widget_id = {{ $widget_id }};//url.searchParams.get("widget_id");

        @if($widget->waiting_image)
            window.waiting_image_url = '/file/' + '{{$widget->waiting_image}}';
        @endif

        const localMediaContainer = document.getElementById('local-media');


        const showPreview = async (roomName, token) => {
            // if(!tracks) {
            //     // tracks = await Twilio.Video.createLocalVideoTrack;
            //     tracks = await Twilio.Video.createLocalTracks;
            // }
            //
            // tracks().then(track => {
            //     localMediaContainer.appendChild(track.attach());
            // });

            const tracks = await Twilio.Video.createLocalTracks();

// Display camera preview.

            // Display camera preview.
            localVideoTrack = tracks.find(track => track.kind === 'video');

            localMediaContainer.innerHTML="";
            localMediaContainer.appendChild(localVideoTrack.attach());
        };

        // showPreview();

        // $('#videoChatContainer').visibilityChanged(function(element, visible) {
        //
        // });




        const checkbox = document.getElementById('enable_camera')
        const video_preview = document.getElementById('local-media')

        // checkbox.addEventListener("visibilitychange", function() {
        //     if (document.visibilityState === 'visible') {
        //         consoel.log('iframe visible')
        //     } else {
        //         console.log('iframe not visible')
        //     }
        // });

        video_preview.addEventListener('click', (event) => {
            showPreview();
        });

        checkbox.addEventListener('change', (event) => {
            if (event.currentTarget.checked) {
                showPreview();
            } else {
                // tracks.stop();
                localVideoTrack.stop();
            }
        });

        window.addEventListener('message', function (event) {
            $msg = event.data.message;
            console.log('called on message');
            // if ($msg == "previewCamera") {
            //
            // }
        }, false);


    </script>
    <script src="https://virtual-assistants.gr/promoter/main.js?v=125"></script>
  </body>
</html>
