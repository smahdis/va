{{--{{ json_encode($widget,TRUE)}}--}}
{{--<script>--}}
    (function (global) {
        (function (w) {
            function l() {
                // console.log('hello');
                var s0 = document.getElementsByTagName("script")[0];

                if (typeof jQuery == 'undefined') {
                    var script = document.createElement('script');
                    script.type = "text/javascript";
                    script.src = "https://code.jquery.com/jquery-3.6.0.min.js";
                    document.getElementsByTagName('head')[0].appendChild(script);
                    s0.parentNode.insertBefore(script, s0);
                }

                var files = [
                    "https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"
                ];

                for (var i = 0; i < files.length; i++) {
                    var s1 = document.createElement("script");
                    s1.src = files[i];
                    s1.charset = "UTF-8";
                    s1.setAttribute("crossorigin", "*");
                    s0.parentNode.insertBefore(s1, s0);
                }

                var iframeBackdrop = document.createElement('div');
                    iframeBackdrop.id = 'iframe-backdrop';
                    iframeBackdrop.style =
                    '-webkit-filter: blur(2px);' +
                    '-moz-filter: blur(2px);' +
                    '-o-filter: blur(2px);' +
                    '-ms-filter: blur(2px);' +
                    'filter: blur(2px);' +
                    'width: 100%;' +
                    'height: 100%;' +
                    'position: fixed;' +
                    'top: 0;' +
                    'bottom: 0;' +
                    'z-index: 100000;' +
                    'background: #0b0b0b80;' +
                    'display: none;' +
                    'opacity: 0.8;' +
                    'backdrop-filter: blur(35px);'
                ;
                document.body.appendChild(iframeBackdrop);

                var iframewrapper = document.createElement('div');
                iframewrapper.id = 'iframe-wrapper';
                iframewrapper.style =
                    'position: fixed; ' +
                    'z-index: 99999999999999999; ' +
                    'background: {{ $widget->widget_color }}; ' +
                    'top: 50%; ' +
                    'left: 50%; -webkit-transform: translateX(-50%) translateY(-50%);' +
                    '-moz-transform: translateX(-50%) translateY(-50%); ' +
                    '-ms-transform: translateX(-50%) translateY(-50%);' +
                    'transform: translateX(-50%) translateY(-50%); ' +
                    'border-radius: 10px !important; ' +
                    'display: flex;' +
                    'align-items: center;' +
                    'justify-content: center;' +
                    'display: none;';
                document.body.appendChild(iframewrapper);

                (function () {
                    var a = setInterval(function () {
                        if (typeof window.jQuery === 'undefined') {
                            return;
                        }
                        clearInterval(a);

{{--                        $.ajax({--}}
{{--                            type: "POST",--}}
{{--                            url: "https://video.demos.ge/promoter/chat.html",--}}
{{--                            success: function (data) {--}}
                                var buttonDiv = document.createElement('div');
                                // buttonDiv.innerHTML = data;
                                buttonDiv.innerHTML = '<a type="button" id="chat-button" tabindex="0" style="' +
                                                                                                    'bottom: 0px;' +
                                                                                                    'left: 0px;' +
                                                                                                    'position: absolute;' +
                                                                                                    'z-index: 1000;' +
                                                                                                    'width: 60px;' +
                                                                                                    'height: 60px;' +
                                                                                                    'line-height: 3.75rem;' +
                                                                                                    'margin: 0;' +
                                                                                                    'overflow: hidden;' +
                                                                                                    'font: inherit;' +
                                                                                                    'text-transform: none;' +
                                                                                                    'display: inline-block;' +
                                                                                                    'padding: 0.5rem 1rem;' +
                                                                                                    'font-size: 1rem;' +
                                                                                                    'border: unset;' +
                                                                                                    'line-height: 1.5rem;' +
                                                                                                    'text-decoration: none;' +
                                                                                                    'background-color: unset;' +
                                                                                                    'cursor: pointer;' +
                                                    '">' +
                                    '<div isroundwidget="true">' +
                                    '<div>' +
                                    '<img src="{{Request::getSchemeAndHttpHost()}}{{ $widget->logo_url }}" style="width: 48px;">' +
                                    '</div>' +
                            '</div>' +
                            '</a>';
                                buttonDiv.id = 'chat-buttton-iframe';
                                buttonDiv.style =
                                    ' outline:none !important;\n' +
                                    ' visibility:visible !important;\n' +
                                    ' resize:none !important;\n' +
                                    ' box-shadow:none !important;\n' +
                                    ' overflow:visible !important;\n' +
                                    ' opacity:1 !important;\n' +
                                    ' filter:alpha(opacity=100) !important;\n' +
                                    ' -ms-filter:progid:DXImageTransform.Microsoft.Alpha(Opacity 1}) !important;\n' +
                                    ' -mz-opacity:1 !important;\n' +
                                    ' -khtml-opacity:1 !important;\n' +

                                    @if ($widget->vertical_position=="bottom")
                                        ' top:auto !important;\n' +
                                        ' bottom:20px !important;\n' +
                                    @elseif($widget->vertical_position=="middle")
                                        ' top:50% !important;\n' +
                                        ' bottom:auto !important;\n' +
                                    @else
                                        ' top:20px !important;\n' +
                                        ' bottom:auto !important;\n' +
                                    @endif

{{--                                    ' top:auto !important;\n' +--}}
                                    @if ($widget->horizontal_position=="right")' right:20px !important;\n'
                                    @else ($widget->horizontal_position=="right")' right:auto !important;\n' @endif +

                                    @if ($widget->horizontal_position=="right")' left:auto !important;\n'
                                    @else ' left:20px !important;\n' @endif +
                                    ' position:fixed !important;\n' +
                                    ' border:0 !important;\n' +
                                    ' min-height:60px !important;\n' +
                                    ' min-width:64px !important;\n' +
                                    ' padding:0 !important;\n' +
                                    ' margin:0 !important;\n' +
                                    ' -moz-transition-property:none !important;\n' +
                                    ' -webkit-transition-property:none !important;\n' +
                                    ' -o-transition-property:none !important;\n' +
                                    ' transition-property:none !important;\n' +
                                    ' transform:none !important;\n' +
                                    ' -webkit-transform:none !important;\n' +
                                    ' -ms-transform:none !important;\n' +
                                    ' width:64px !important;\n' +
                                    ' height:60px !important;\n' +
                                    ' display:block !important;\n' +
                                    ' z-index:1000001 !important;\n' +
                                    ' cursor:none !important;\n' +
                                    ' float:none !important;\n' +
                                    ' border-radius:unset !important;\n' +
                                    ' pointer-events:auto !important;\n' +
                                    ' clip:auto !important;\n' +
                                    ' color-scheme:light !important;';

                                document.body.appendChild(buttonDiv);

                                document.getElementById("chat-buttton-iframe").onclick =
                                    function () {
                                        $('#iframe-wrapper').toggle();
                                        $('#iframe-backdrop').toggle();
{{--                                        let iframeElement = document.getElementById('above-chat-box');--}}
{{--                                        window.postMessage('hi!');--}}

                                    }

{{--                            }--}}
{{--                        });--}}


                    }, 500);
                })();


                document.getElementById("chat-buttton-iframe");


                var iframeWrapper = document.getElementById('iframe-wrapper');

                var iframeClose = document.createElement('a');
                iframeClose.href = "#";
                iframeClose.innerHTML = "×";
                iframeClose.className = "close";
                iframeClose.id = "closeRoom";


                iframeClose.onclick = function (e) {
                    e.preventDefault();
                    this.parentElement.style.display = 'none';
                    $('#iframe-backdrop').toggle();
{{--                    location.reload();--}}
                    document.getElementById('iframe-wrapper').querySelector("iframe").src+="";

                };

                iframeClose.style = 'position: absolute;\n' +
                    'right: 21px;\n' +
                    'top: 0px;\n' +
                    'color: #fff;\n' +
                    'font-size: 35px;';

                iframeWrapper.appendChild(iframeClose);

                var iframe2 = document.createElement('iframe');
{{--                iframe2.src = 'https://virtual-assistants.gr/promoter/index.html?widget_id={{$widget->id}}';--}}
                iframe2.src = 'https://virtual-assistants.gr/admin/client/index/{{$widget->id}}';

                iframe2.className = 'iframe';
                iframe2.width = '800px';
                iframe2.height = '520px';
                iframe2.allow = 'camera;microphone';
                iframe2.id = 'above-chat-box';
{{--                iframe2.style = 'padding: 20px !important;border-radius: 20px !important;outline:none !important;visibility:visible !important;resize:none !important; !important;overflow:visible !important; opacity:1 !important;filter:alpha(opacity=100) !important;-ms-filter:progid:DXImageTransform.Microsoft.Alpha(Opacity 1}) !important;-mz-opacity:1 !important;-khtml-opacity:1 !important; top:auto !important; right:10px !important; bottom:90px !important; left:auto !important; border:0 !important;min-height:520px !important;min-width:350px !important;max-height:600px !important;max-width:800px !important; margin:0 !important;-moz-transition-property:none !important;-webkit-transition-property:none !important;-o-transition-property:none !important; transition-property:none !important; transform:none !important;-webkit-transform:none !important;-ms-transform:none !important;width:800px !important;height:600px !important; z-index:auto !important; cursor:none !important; float:none !important; pointer-events:auto !important; clip:auto !important; color-scheme:light !important;';--}}
                iframe2.style = 'padding: 45px 15px 15px 15px !important;border-radius: 20px !important;outline: none !important;visibility: visible !important;/* resize: none !important; */overflow: visible !important;opacity: 1 !important;inset: auto 10px 90px auto !important;border: 0px !important;/* min-height: 520px !important; *//* min-width: 350px !important; *//* max-height: 870px !important; */max-width: 100vw !important;margin: 0px !important;transition-property: none !important;transform: none !important;width: 75vw !important;height: 86vh !important;z-index: auto !important;cursor: none !important;float: none !important;pointer-events: auto !important;clip: auto !important;color-scheme: light !important;padding-top: 50px;';
                iframeWrapper.appendChild(iframe2);


            }

            (function () {
                var a = setInterval(function () {
                    if (typeof window.jQuery === 'undefined') {
                        return;
                    }
                    clearInterval(a);
                    l();
                }, 500);
            })();

            window.addEventListener('message', function (event) {
                $msg = event.data.message;
                if ($msg == "closeIfram") {

                }


            }, false);

        })(window);
    })(window);


{{--</script>--}}
