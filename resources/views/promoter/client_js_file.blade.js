    (function (global) {
        (function (w) {
            function l() {
                console.log('hello');
                var s0 = document.getElementsByTagName("script")[0];

                if (typeof jQuery == 'undefined') {
                    var script = document.createElement('script');
                    script.type = "text/javascript";
                    script.src = "https://code.jquery.com/jquery-3.6.0.min.js";
                    document.getElementsByTagName('head')[0].appendChild(script);
                    s0.parentNode.insertBefore(script, s0);
                }

                var files = [
                    "https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"
                ];

                for (var i = 0; i < files.length; i++) {
                    var s1 = document.createElement("script");
                    s1.src = files[i];
                    s1.charset = "UTF-8";
                    s1.setAttribute("crossorigin", "*");
                    s0.parentNode.insertBefore(s1, s0);
                }

                var iframeBackdrop = document.createElement('div');
                    iframeBackdrop.id = 'iframe-backdrop';
                    iframeBackdrop.style =
                    '-webkit-filter: blur(2px);' +
                    '-moz-filter: blur(2px);' +
                    '-o-filter: blur(2px);' +
                    '-ms-filter: blur(2px);' +
                    'filter: blur(2px);' +
                    'width: 100%;' +
                    'height: 100%;' +
                    'position: fixed;' +
                    'top: 0;' +
                    'bottom: 0;' +
                    'z-index: 100000;' +
                    'background: #0b0b0b80;' +
                    'display: none;' +
                    'opacity: 0.8;' +
                    'backdrop-filter: blur(35px);'
                ;
                document.body.appendChild(iframeBackdrop);

                var iframewrapper = document.createElement('div');
                iframewrapper.id = 'iframe-wrapper';
                iframewrapper.style = ' position: fixed; z-index: 99999999999999999; background: #202124; top: 50%; left: 50%; -webkit-transform: translateX(-50%) translateY(-50%); -moz-transform: translateX(-50%) translateY(-50%); -ms-transform: translateX(-50%) translateY(-50%); transform: translateX(-50%) translateY(-50%); border-radius: 20px !important; display: flex;align-items: center;justify-content: center;display: none;';
                document.body.appendChild(iframewrapper);

                (function () {
                    var a = setInterval(function () {
                        if (typeof window.jQuery === 'undefined') {
                            return;
                        }
                        clearInterval(a);

                        // $('#iframe-wrapper').toggle();
                        // $('#iframe-backdrop').toggle();

                    }, 500);
                })();


                var iframeWrapper = document.getElementById('iframe-wrapper');

                var iframeClose = document.createElement('a');
                iframeClose.href = "#";
                iframeClose.innerHTML = "×";
                iframeClose.className = "close";
                iframeClose.id = "closeRoom";
                iframeClose.onclick = function (e) {
                    e.preventDefault();
                    this.parentElement.style.display = 'none';
                    $('#iframe-backdrop').toggle();
                };

                iframeClose.style = 'position: absolute;\n' +
                    'right: 21px;\n' +
                    'top: 0px;\n' +
                    'color: #fff;\n' +
                    'font-size: 35px;';

                iframeWrapper.appendChild(iframeClose);

                var iframe2 = document.createElement('iframe');
                iframe2.src = 'https://virtual-assistants.gr/promoter/index.html';

                iframe2.className = 'iframe';
                iframe2.width = '800px';
                iframe2.height = '520px';
                iframe2.allow = 'camera;microphone';
                iframe2.id = 'above-chat-box';
                iframe2.style = 'padding: 45px 15px 15px 15px !important;border-radius: 20px !important;outline: none !important;visibility: visible !important;/* resize: none !important; */overflow: visible !important;opacity: 1 !important;inset: auto 10px 90px auto !important;border: 0px !important;/* min-height: 520px !important; *//* min-width: 350px !important; *//* max-height: 870px !important; */max-width: 100vw !important;margin: 0px !important;transition-property: none !important;transform: none !important;width: 75vw !important;height: 86vh !important;z-index: auto !important;cursor: none !important;float: none !important;pointer-events: auto !important;clip: auto !important;color-scheme: light !important;padding-top: 50px;';
                iframeWrapper.appendChild(iframe2);


            }

            (function () {
                var a = setInterval(function () {
                    if (typeof window.jQuery === 'undefined') {
                        return;
                    }
                    clearInterval(a);
                    l();
                }, 500);
            })();

        })(window);
    })(window);
