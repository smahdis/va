<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CustomAuthController;
use App\Http\Controllers\StoreController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/promoter', function () {
    return view('welcome');
});

Route::group(['middleware' => 'auth'], function(){
//    Route::get('/admin', 'App\Http\Controllers\AdminController@index');

});

Route::group(['middleware' => ['auth', 'role']], function() {

    Route::get('/admin', 'App\Http\Controllers\StoreController@index');
    Route::get('/admin/stores', ['as' => '/admin/stores','middleware' => 'auth', 'uses' => 'App\Http\Controllers\StoreController@index']);
    Route::get('/admin/stores/add', ['as' => 'add-store','middleware' => 'auth', 'uses' => 'App\Http\Controllers\StoreController@addForm']);
    Route::post('/admin/stores/store', ['as' => 'add-store-submit','middleware' => 'auth', 'uses' => 'App\Http\Controllers\StoreController@store']);
    Route::delete('/admin/stores/delete/{store_id}', ['as' => 'delete-store','middleware' => 'auth', 'uses' => 'App\Http\Controllers\StoreController@destroy']);
    Route::get('/admin/stores/edit/{store_id}', ['as' => 'edit-store','middleware' => 'auth', 'uses' => 'App\Http\Controllers\StoreController@edit']);
    Route::put('/admin/stores/update/{store_id}', ['as' => 'update-store','middleware' => 'auth', 'uses' => 'App\Http\Controllers\StoreController@update']);


    Route::get('/admin/widgets/create/{store_id}', ['as' => 'create-widget','middleware' => 'auth', 'uses' => 'App\Http\Controllers\WidgetController@create']);
    Route::post('/admin/widgets/save/{store_id}', ['as' => 'save-widget','middleware' => 'auth', 'uses' => 'App\Http\Controllers\WidgetController@save']);
    Route::get('/admin/widgets/{store_id}', ['as' => 'widgets','middleware' => 'auth', 'uses' => 'App\Http\Controllers\WidgetController@index']);
    Route::get('/admin/widgets/schedule/create/{store_id}', ['as' => 'schedule','middleware' => 'auth', 'uses' => 'App\Http\Controllers\WidgetController@scheduleCreate']);
    Route::post('/admin/widgets/schedule/save/{store_id}', ['as' => 'save-schedule','middleware' => 'auth', 'uses' => 'App\Http\Controllers\WidgetController@scheduleSave']);
    Route::get('/admin/widgets/edit/{widget_id}', ['as' => 'edit-widget','middleware' => 'auth', 'uses' => 'App\Http\Controllers\WidgetController@edit']);
    Route::post('/admin/widgets/update/{widget_id}/{store_id}', ['as' => 'update-widget','middleware' => 'auth', 'uses' => 'App\Http\Controllers\WidgetController@update']);

    Route::get('/admin/users/create/{store_id}', ['as' => 'create-user','middleware' => 'auth', 'uses' => 'App\Http\Controllers\UserController@create']);
    Route::post('/admin/users/save/{store_id}', ['as' => 'save-user','middleware' => 'auth', 'uses' => 'App\Http\Controllers\UserController@save']);
    Route::get('/admin/users/{store_id}', ['as' => 'users','middleware' => 'auth', 'uses' => 'App\Http\Controllers\UserController@index']);
    Route::get('/admin/promoters/edit/{user_id}', ['as' => 'edit-promoter','middleware' => 'auth', 'uses' => 'App\Http\Controllers\UserController@edit']);
    Route::post('/admin/promoters/update/{user_id}', ['as' => 'update-promoter','middleware' => 'auth', 'uses' => 'App\Http\Controllers\UserController@update']);
    Route::get('/admin/promoters/delete/{user_id}', ['as' => 'delete-promoter','middleware' => 'auth', 'uses' => 'App\Http\Controllers\UserController@destroy']);





});

Route::post('/admin/promoters/update-background', ['as' => 'update-background','middleware' => 'auth', 'uses' => 'App\Http\Controllers\UserController@updateBackground']);
Route::get('/admin/promoters/recordings', ['as' => 'recordings','middleware' => 'auth', 'uses' => 'App\Http\Controllers\API\TwilioController@listCompositions']);
Route::get('/admin/promoters/downloadComposition/{compositionSid}', ['as' => 'downloadComposition','middleware' => 'auth', 'uses' => 'App\Http\Controllers\API\TwilioController@downloadComposition']);

Route::get('/admin/widgets/get/{widget_id}', ['as' => 'get-widget', 'uses' => 'App\Http\Controllers\WidgetController@get']);
Route::get('/admin/widgets/getRooms/{store_id}', ['as' => 'get-rooms', 'uses' => 'App\Http\Controllers\WidgetController@getRooms']);
Route::get('/admin/stores/list', ['as' => 'get-stores', 'uses' => 'App\Http\Controllers\StoreController@getStores']);


/*
 * Client Side Stuff
 */
Route::get('/admin/client/index/{widget_id}', ['as' => 'client-index', 'uses' => 'App\Http\Controllers\ClientSideController@index']);


//Route::get('dashboard', [AuthController::class, 'dashboard']);
Route::get('login', [AuthController::class, 'index'])->name('login');
Route::post('custom-login', [AuthController::class, 'customLogin'])->name('login.custom');
Route::get('register', [AuthController::class, 'register'])->name('register-user');
Route::post('custom-registration', [AuthController::class, 'customRegistration'])->name('register.custom');
Route::post('signout', [AuthController::class, 'signOut'])->name('signout');

//Route::get('config/{widget_id?}', ['middleware' => 'cors', StoreController::class, 'returnStoreJsFile'])->name('config');
Route::get('config/{widget_id?}', ['as' => 'config', 'uses' =>'App\Http\Controllers\StoreController@returnStoreJsFile']);

Route::group(['middleware' => 'auth'], function(){
//    Route::get('/admin', 'App\Http\Controllers\AdminController@index');
    Route::get('/', 'App\Http\Controllers\AdminController@index')->name('/');
    Route::get('/admin/preview/{widget_id}/{room_name}', ['as' => 'preview','middleware' => 'auth', 'uses' =>'App\Http\Controllers\AdminController@preview']);
    Route::get('/admin/connect/{widget_id?}/{room_name?}', ['as' => 'connect','middleware' => 'auth', 'uses' =>'App\Http\Controllers\AdminController@connect']);//->name('connect');
    Route::get('/admin/leaveRoom/{room_name}/{room_name_base64?}', ['as' => 'leaveRoom','middleware' => 'auth', 'uses' =>'App\Http\Controllers\AdminController@leaveRoom']);//->name('connect');

});

Route::get('/statusCallback', ['as' => 'statusCallback', 'uses' =>'App\Http\Controllers\API\TwilioController@statusCallback']);//->name('connect');
Route::post('/statusCallback', ['as' => 'statusCallback', 'uses' =>'App\Http\Controllers\API\TwilioController@statusCallback']);//->name('connect');

//Auth::routes();
//
//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
