<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::post('joinRoom/{widget_id?}', 'App\Http\Controllers\API\TwilioController@joinRoom');
Route::post('joinRoom/{widget_id?}', array( 'uses' => 'App\Http\Controllers\API\TwilioController@joinRoom'));
//Route::post('joinRoom/{widget_id?}', array('middleware' => 'cors', 'uses' => 'App\Http\Controllers\API\TwilioController@joinRoom'));
