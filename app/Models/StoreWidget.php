<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StoreWidget extends Model
{
    use HasFactory;


    public function insertSchedule($params): array
    {

        $p1 = json_encode([
            'store_id'		        => $params['store_id']
        ]);

        $p2 = json_encode([
            'schedules' => $this->concateItems($params['schedule'])
        ]);

        $result = DB::select('call schedule_insert(?,?)',array($p1,$p2));

        return $result;
    }

    private function concateItems($items)
    {
        $return = array();
        if(empty($items))
        {
            return '';
        }
        foreach($items as $key => $item)
        {
//            var_dump($item);
//            die();
            foreach($item as $key1 => $it) {
                $return[] = array(
                    ($key),
                    $key1,
    //                (empty($item['day_title']) ? 'NULL' : $item['day_title']),
                    (isset($item[0]['status'])) ? 1 : 0,
                    ($it['start']),
                    ($it['end']),
                );
            }
        }


        return $this->getForMysql($return);
    }

    /**
     * Format array as below format
     *  "field" , "field" , "field" ; "field" , "field" , "field" ; "field" , "field" , "field"
     *
     * @param $array
     * @param $appendQuota
     * @return string
     */
    public static function getForMysql($array, $appendQuota = true, $arraySplit = ';'): string
    {
        if(empty($array))
        {
            return '';
        }
        $return = array();

        foreach($array as $row)
        {
            array_walk($row, function(&$value) use ($appendQuota){
//                $value = \DataSource::getInstance()->getPdo()->quote($value);

                if((is_numeric($value) === false || str_starts_with($value, '0')) and $appendQuota)
                {
                    // Must Check influence of it in project
                    //      if(strtolower($value) != 'null')
                    //     {
                    $value = '"' . $value . '"';
                    //     }
                }
            });
            $return[] = implode(',', $row);
        }
        return implode($arraySplit, $return);
    }


}
