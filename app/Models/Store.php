<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Store extends Model
{
    use HasFactory;
    protected $primaryKey = 'store_id_pk';
    /**
     * Get the timezone.
     */
    public function timezone(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Timezone::class, 'timezone_id', 'timezone_id_pk');
    }

    /**
     * Get the scheduleConfig.
     */
    public function scheduleConfig(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(ScheduleConfig::class, 'schedule_config_id', 'schedule_config_id_pk');
    }

    /**
     * Get the store by domain name.
     */
    public function storeByDomainName($params)
    {
        $p1 = json_encode([]);

        $p2 = json_encode([
            'domain_name' => $params['domain_name']
        ]);

        DB::enableQueryLog();

        $result = DB::select('call get_store_by_domain_name(?,?)',array($p1,$p2));

        $queries = DB::getQueryLog();

//        var_dump($queries);
//        die();
        return $result;
    }

}
