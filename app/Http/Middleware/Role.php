<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class Role {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ... $roles)
    {

        if (! Auth::check()) {
            return redirect()->route('login');
        }

        $user = Auth::user();

        if($user->isAdmin())
            return $next($request);

        if ($user->isPromoter()) {
            return redirect()->route('/');
        }

//        foreach($roles as $role) {
//            // Check if user has the role This check will depend on how your roles are set up
//            if($user->hasRole($role))
//                return $next($request);
//        }

        return redirect('login');
    }

}
