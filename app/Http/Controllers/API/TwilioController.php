<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Store;
use App\Models\StoreWidget;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use League\Flysystem\Exception;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Exceptions\RestException;
use Twilio\Exceptions\TwilioException;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;
use Twilio\Rest\Client;
use Twilio\Rest\Video;

class TwilioController extends Controller
{

    private $accountSid;
    private $apiKeySid;
    private $apiKeySecret;
    private $authToken;
    private $twilio;

    public function __construct()
    {
        $this->accountSid = env('TWILIO_ACCOUNT_SID');
        $this->apiKeySid = env('TWILIO_API_KEY_SID');
        $this->apiKeySecret = env('TWILIO_API_KEY_SECRET');
        $this->authToken = env('TWILIO_AUTH_TOKEN');
//        $this->twilio = new Client($this->accountSid, $this->authToken);
        $this->twilio = new Client($this->apiKeySid, $this->apiKeySecret, $this->accountSid);
    }

    public static function createSlug($str, $delimiter = '-'){
        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;
    }

    public function statusCallback(Request $request)
    {
        if(isset($request->all()['StatusCallbackEvent'])) {
            $callbackevent = $request->all()['StatusCallbackEvent'];
        }

        if(isset($callbackevent) && ($callbackevent == "participant-connected" || $callbackevent == "participant-disconnected"))
            Log::info($request->all()['RoomName']);
            Log::info(base64_decode($request->all()['RoomName']));
//            Log::info($request->all());
        die();
    }

    public function leaveRoom($room_name)
    {

        User::where('id',Auth::user()->id)->update([
            "room_name_base64"            => null,
        ]);

        try {
            $room = $this->twilio->video->v1->rooms($room_name)->update("completed");
//            $room->statusCallback = "https://virtual-assistants.gr/statusCallback";
        } catch (TwilioException $e) {
            Log::error($e);
        }
    }

    public function joinRoom(Request $request, $widget_id): array
    {
        $data = $request->all();

        $roomName = base64_encode($_SERVER['SERVER_NAME'] . $widget_id);

        $response = Http::get('https://virtual-assistants.gr/admin/widgets/get/' . $widget_id);
        $response = $response->object();
        $roomName = $this->retreiveRoomName($response);

        if($roomName=="") {
            return [];
        }

        try {
            $room = $this->twilio->video->v1->rooms($roomName)->fetch();
        } catch (TwilioException $e) {
            if($e->getCode() == 20404) {
                $room = $this->twilio->video->v1->rooms
                    ->create([
//                        [
                            "Type" => "group",
                            "UniqueName" => $roomName,
//                            "RecordParticipantsOnConnect" => true,
                            "statusCallback" => "https://virtual-assistants.gr/statusCallback",
                            "StatusCallbackMethod" => "GET"
//                        ]

                    ]);



            } else {
                // let other errors bubble up
                throw $e;
            }
        }

        if(Auth::check()) {
//            $identity = 'admin' . uniqid();
            $identity = [
                "id" => uniqid(),
                "full_name" => Auth::user()->full_name,
                "is_promoter" => 1,
                "user_id" => Auth::user()->id
            ];

            $recording_rules = $this->twilio->video->v1->rooms($room->sid)
                ->recordingRules
                ->update(["rules" => [["type" => "include", "all" => true]]]);
//                ->update(["rules" => [["type" => "include", "publisher" => json_encode($identity)]]]);
            $compositionHook = $this->twilio->video->compositionHooks->create(
                'MyFirstCompositionHook' . time(), [
                'audioSources' => '*',
                'videoLayout' =>  array(
                    'grid' => array (
                        'video_sources' => array('*')
                    )
                ),
                'statusCallback' => 'https://virtual-assistants.gr/statusCallback',
                'format' => 'mp4'
            ]);

        } else {
            $identity = [
                "id" => uniqid(),
                "full_name" => !empty($data['firstName']) ? $data['firstName'] : 'User-' . rand(1, 550)
            ];
        }

        // Create an Access Token
        $token = new AccessToken(
            $this->accountSid,
            $this->apiKeySid,
            $this->apiKeySecret,
            3600,
            json_encode($identity)
        );

        // Grant access to Video
        $grant = new VideoGrant();
        $grant->setRoom($roomName);
        $token->addGrant($grant);

//        User::where('id',Auth::user()->id)->update([
//            "room_name_base64"            => $roomName,
//        ]);

        // Serialize the token as a JWT
        return ["token" => $token->toJWT(), "roomName" => $roomName];
    }

    public function getRoomsList() {
        $rooms = $this->twilio->video->v1->rooms
            ->read(["status" => "in-progress"], 20);

        foreach ($rooms as $record) {
            $rec = $this->twilio->video->v1->rooms($record->uniqueName)->participants->read(['status' => 'connected']);
            var_dump($rec[0]);

        }

        die();

        foreach ($rooms as $record) {
            print_r($record->uniqueName);
        }
    }

    public function listCompositions() {
        $compositions = $this->twilio->video->compositions
            ->read([
                'status' => "completed"
            ]);

        return view('admin.compositions.list', [
            'compositions' => $compositions,
        ]);


    }

    public function downloadComposition($compositionSid) {
//        $url=$_POST['https://video.twilio.com/v1/Compositions/CJ236a0d64d05947c7d4ab26b76eac836f/Media'];
//        $filename=$_POST['name'];
//
//        header('Content-type: video/mp4');
//        header('Content-Disposition: attachment; filename="'."CJ236a0d64d05947c7d4ab26b76eac836f.mp4".'"');
//
//        echo file_get_contents($url);

//        $compositionSid = "CJ236a0d64d05947c7d4ab26b76eac836f";
        $uri = "https://video.twilio.com/v1/Compositions/$compositionSid/Media?Ttl=3600";
        $response = $this->twilio->request("GET", $uri);
        $mediaLocation = $response->getContent()["redirect_to"];

        $file_name = "$compositionSid" . ".mp4";
        $file_url = $mediaLocation;
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"".$file_name."\"");
        readfile($file_url);
        exit;

    }


    private function retreiveRoomName($widget_store): string
    {
        $store_name_slug = $this->createSlug($widget_store->store->store_title);
        $roomName = $store_name_slug . '-' . $widget_store->widget->room_name;

        if(Auth::check()) {
            $roomName = $roomName . '-' . Auth::user()->id;
//            var_dump($roomName);
//            die();
            return base64_encode($roomName);
        }


        $users = (new User)->where('current_room', $widget_store->widget->room_name)->where('is_promoting', 1)->get();
        $rooms = [];


//        var_dump($users);
//        die();

        foreach ($users as $key => $user) {
//            var_dump($user->id);
            $room_to_check = base64_encode($store_name_slug . '-' .$widget_store->widget->room_name . '-' . $user->id);

//            var_dump($store_name_slug . '-' .$widget_store->widget->room_name . '-' . $user->id);
//            die();

            try {
                $room = $this->twilio->video->v1->rooms($room_to_check)->fetch();
                $participants = $room->participants->read(['status' => 'connected']);
                if(count($participants) < $user->room_limit ) {
                    $rooms[$key]['room_name'] = $room_to_check;
                    $rooms[$key]['count'] = count($participants);
                }
            } catch(RestException $e) {
//                echo $store_name_slug . '-' .$widget_store->widget->room_name . '-' . $user->id . ' doesnt exist', PHP_EOL;
            }
        }


        usort($rooms, function ($item1, $item2) {
            return $item2['price'] <=> $item1['price'];
        });

        if($rooms)
            return $rooms[0]['room_name'];
        else
            return "";


    }

    public function checkIfRoomHasParticipants($widget_id) {

        $widget = (new StoreWidget())->where('id', $widget_id)->first();
        $store = (new Store)->where('store_id_pk', $widget->store_id)->first();

        $store_name_slug = (new TwilioController())->createSlug($store->store_title);
        $roomName = $store_name_slug . '-' . $widget->room_name;

        $users = (new User)->where('widget_id', $widget_id)->where('is_promoting', 1)->get();
        $rooms = [];

//        var_dump($users);
//        die();

        foreach ($users as $key => $user) {
//            var_dump($user->id);
            $room_to_check = base64_encode($store_name_slug . '-' .$widget->room_name . '-' . $user->id);

//            var_dump($store_name_slug . '-' .$widget->room_name . '-' . $user->id);
//            die();

            try {
                $room = $this->twilio->video->v1->rooms($room_to_check)->fetch();
                $participants = $room->participants->read(['status' => 'connected']);
                if(count($participants) < $user->room_limit ) {
                    $rooms[$key]['room_name'] = $room_to_check;
                    $rooms[$key]['count'] = count($participants);

                    return true;
                }
            } catch(RestException $e) {
//                echo $store_name_slug . '-' .$widget_store->widget->room_name . '-' . $user->id . ' doesnt exist', PHP_EOL;
            }
        }

        return false;
    }
}
