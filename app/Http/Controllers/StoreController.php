<?php

namespace App\Http\Controllers;

use App\Http\Controllers\API\TwilioController;
use App\Models\ScheduleConfig;
use App\Models\Store;
use App\Models\StoreWidget;
use App\Models\Timezone;
use App\Models\User;
use App\Models\Widget;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\Input;
use Twilio\Exceptions\RestException;

class StoreController extends Controller
{
    public function index() {
        $stores = Store::where('status', 1)->get();

        return view('admin.store.index',["stores"=>$stores]);
//        return Response::json($stores);
    }

    public function addForm(Request $request) {
        $timezones = Timezone::all();
        $store = new Store();
        $schedule_configs  = (new ScheduleConfig())->where('is_general', 1)->get();
        return view('admin.store.add')->with(compact(['store', $store, 'timezones', $timezones, 'schedule_configs', $schedule_configs]));
    }

    public function store(Request $request) {
        $data = $request->all();
        $store = new Store();
        $store->timezone_id = $data['timezone_id'];
        $store->store_title = $data['store_title'];
//        $store->working_hours_from = $data['working_hours_from'];
//        $store->working_hours_to = $data['working_hours_to'];
        $store->email = $data['email'];
        $store->schedule_config_id = $data['schedule_config_id'];
//        $store->country = $data['country'];
//        $store->city = $data['city'];
        $store->website = $data['website'];

        $fileName="";
        $uploadedFile = $request->file('store_logo');
        if(!empty($uploadedFile)) {
            $fileName = auth()->id() . '_' . time() . '.' . $uploadedFile->extension();
            if ($uploadedFile->isValid()) {
                $uploadedFile->move(public_path('file'), $fileName);
            }
        }
        $store->store_logo_url = $fileName;
        $store->save();
//        Store::create($request->all());
        return redirect('/admin/stores');
    }

    public function getStores() {
        $stores = (new Store)->where('status', 1)->get();
        return $stores;
    }

    public function destroy($id) {
        Store::where('store_id_pk',$id)->update(['status'=>-1]);
        return redirect('/admin/stores')->with('message', 'Store deleted successfully');
    }

    public function edit($store_id) {

        $store = Store::where('store_id_pk', $store_id)->first();
        $timezones = Timezone::all();
        $schedule_configs  = (new ScheduleConfig())->where('store_id', $store_id)->orWhere('is_general', 1)->get();

        return view('admin.store.add')->with(compact(['store', $store, 'timezones', $timezones,'schedule_configs', $schedule_configs]));
    }

    public function returnStoreJsFile(Request $request, $widget_id=null) {
        if(request()->headers->get('referer')) {
//            var_dump($_SERVER['HTTP_REFERER']);
//            var_dump(request()->headers->get('referer'));
//            die();
            $url = request()->headers->get('referer');
            $urlParts = parse_url($url);
            $domain_name = preg_replace("(^https?://)", "", $url );#parse_url($_SERVER['HTTP_REFERER'])['host'];
            $domain_name = preg_replace('#^(http(s)?://)?w{3}\.#', '$1', $urlParts['host']);


            $domain_name = rtrim($domain_name, '/');


            $result = (new Store())->storeByDomainName([
               "domain_name" => $domain_name
           ]);



            $promoterExist = (new TwilioController())->checkIfRoomHasParticipants($widget_id);

//            var_dump($promoterExist);
//            var_dump($result);
//            var_dump($domain_name);
//            die();

            if($result[0]->result == 1 && $promoterExist) {
                if(isset($widget_id)) {
                    $widget = (new StoreWidget())->where('id', $widget_id)->first();
                } else {
                    $widget = (new StoreWidget())->where('store_id', $result[0]->store_id)->first();
                }



                if($widget) {
                    return response()->view('promoter.client_js_file', ["store" => $result[0], "widget" => $widget])
                        ->header('Content-Type', 'application/javascript');
                }
            }
        }

        return response()->json('')
            ->header('Content-Type', 'application/javascript');

    }



    public function update(Request $request, $id) {
        $data = $request->all();
        Store::where('store_id_pk',$id)->update([
            'store_title'=> $data['store_title'],
//            'working_hours_from'=> $data['working_hours_from'],
//            'working_hours_to'=> $data['working_hours_to'],
            'schedule_config_id'=> $data['schedule_config_id'],
            'email'=> $data['email'],
            'timezone_id'=> $data['timezone_id'],
            'website'=> $data['website'],
        ]);

        $fileName="";

        if($data['upload_file_changed']) {
            $uploadedFile = $request->file('store_logo');
//            var_dump($uploadedFile);
//            die();

            if (!empty($uploadedFile)) {
                $fileName = auth()->id() . '_' . time() . '.' . $uploadedFile->extension();
                if ($uploadedFile->isValid()) {
                    $upload_result = $uploadedFile->move(public_path('file'), $fileName);

                }
            }

            Store::where('store_id_pk',$id)->update([
                'store_logo_url'=> $fileName,
            ]);
        }

        return redirect('/admin/stores')->with('message', 'Store updated successfully');

//        return view('admin.store.add')->with(compact(['store', $store, 'timezones', $timezones]));

    }

}
