<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }

    public function customLogin(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');
        $result = Auth::attempt($credentials);

        if ($result) {
//            var_dump('logged in');
//            die();

            return redirect()->intended('/admin')
                ->withSuccess('Signed in');
        }
//        else {
//            var_dump($result);
//            die();
//        }

        return redirect("login")->withSuccess('Login details are not valid');
    }

    public function register()
    {
        return view('auth.register');
    }

    public function customRegistration(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
        ]);

        $data = $request->all();
        $check = $this->create($data);

        return redirect("admin")->withSuccess('You have signed-in');
    }

    public function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
        ]);
    }

//    public function dashboard()
//    {
//        if(Auth::check()){
//            return view('dashboard');
//        }
//
//        return redirect("login")->withSuccess('You are not allowed to access');
//    }

    public function signOut() {
        Session::flush();
        Auth::logout();

        return Redirect('login');
    }
}
