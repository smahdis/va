<?php

namespace App\Http\Controllers;

use App\Models\Store;
use App\Models\StoreWidget;
use App\Models\Timezone;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;

class ClientSideController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index($widget_id)
    {
        $widget = StoreWidget::where('id', $widget_id)->first();
        $store = Store::where('store_id_pk', $widget->store_id)->first();
        return view('admin.clientside.index',["widget_id" => $widget_id, "widget" => $widget, "store" => $store]);
    }

}
