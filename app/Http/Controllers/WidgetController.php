<?php

namespace App\Http\Controllers;

use App\Models\Schedule;
use App\Models\ScheduleConfig;
use App\Models\Store;
use App\Models\StoreWidget;
use App\Models\Timezone;
use App\Models\WeekDay;
use App\Models\Widget;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WidgetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index($store_id)
    {
        $widgets = StoreWidget::where('store_id', $store_id)->get();
        return view('admin.widget.index',["widgets"=>$widgets, "store_id" => $store_id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create($store_id)
    {
        $widget = new StoreWidget();
        $store = Store::where('store_id_pk', $store_id)->first();
        return view('admin.widget.create')->with(compact(['widget', $widget, 'store', $store]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function save(Request $request, $store_id)
    {
        $data = $request->all();
        $uploadedFile = $request->file('logo');

        $fileName="";
        if(!empty($uploadedFile)) {
            $fileName = auth()->id() . '_' . time() . '.' . $uploadedFile->extension();
            if ($uploadedFile->isValid()) {
                $uploadedFile->move(public_path('file'), $fileName);
            }
        }


        $fileName1="";

        $uploadedFile1 = $request->file('waiting_image');
        if (!empty($uploadedFile1)) {
            $fileName1 = auth()->id() . '_' . time() . '.' . $uploadedFile1->extension();
            if ($uploadedFile1->isValid()) {
                $uploadedFile1->move(public_path('file'), $fileName1);
            }
        }

        $widget = new StoreWidget();
        $widget->store_id = $store_id;
        $widget->widget_type = $data['widget_type'];
        $widget->widget_color = $data['widget_color'];
        $widget->vertical_position = $data['vertical_position'];
        $widget->horizontal_position = $data['horizontal_position'];

        $widget->logo_url = '/file/' . $fileName;
        $widget->waiting_image = $fileName1;

//        $widget->generated_code = $data['generated_code'];

        $widget->status = 1;
        $widget->save();

        $widget->room_name = empty($data['room_name']) ? 'room-' . $widget->id : $data['room_name'];

        $widget->generated_code = '(function () {' .
            'var s1 = document.createElement("script"),' .
            '    s0 = document.getElementsByTagName("script")[0];' .
            's1.async = true;' .
            's1.src = "https://virtual-assistants.gr/config/' . $widget->id . '";' .
            's1.charset = "UTF-8";' .
            's1.setAttribute("crossorigin", "*");' .
            's0.parentNode.insertBefore(s1, s0);' .
            '})();';
        $widget->save();
        return redirect('/admin/widgets/' . $store_id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $store_id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function getRooms($store_id)
    {
        DB::statement("SET SQL_MODE=''");
        $rooms = (new StoreWidget())
                ->where('status', 1)
                ->where('store_id', $store_id)
                ->groupBy('room_name')
                ->get();
//        $rooms = $rooms->groupBy('room_name');
        return $rooms;
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $store_id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function scheduleCreate($store_id)
    {
        $week_days = WeekDay::all();
        $schedules = Schedule::where('store_id',$store_id)->whereIn('status', [1,0])
//            ->orWhere(['store_id' => $store_id,'status' => 0])
            ->orderBy('week_day_id_fk', 'ASC')->get();

//        where(function ($query) {
//            $query->where('store_id', '=', $store_id)
//                ->orWhere('status', '=', 1);
//        })->where(function ($query) {
//            $query->where('c', '=', 1)
//                ->orWhere('d', '=', 1);
//        });



        $m_schedules = [];

        foreach ($schedules as $schedule) {
            $m_schedules[$schedule['week_day_id_fk']][] = $schedule;
        }

//        var_dump($m_schedules);
//        die();

        return view('admin.widget.schedule', ['store_id' => $store_id, "week_days" => $week_days, "schedules" => $m_schedules]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $store_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function scheduleSave(Request $request, $store_id)
    {
        $data = $request->all();
//        var_dump($data['schedule']);
//        die();
        $data['store_id'] = $store_id;
        $res = (new StoreWidget())->insertSchedule($data);



        return redirect()->route('edit-store', ['store_id' => $store_id])->with('message', 'Schedule saved successfully');

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Return the specified resource.
     *
     * @param  int  $widget_id
     * @return array
     */
    public function get($widget_id)
    {
        $widget = (new StoreWidget())->where('id', $widget_id)->first();
        $store = (new Store)->where('store_id_pk', $widget->store_id)->first();
//        var_dump($widget);
//        die();
        return compact(['widget', $widget, 'store', $store]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit($widget_id)
    {
        $widget = (new StoreWidget())->where('id', $widget_id)->first();
        return view('admin.widget.create')->with(compact(['widget', $widget]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $widget_id, $store_id)
    {
        $data = $request->all();

        (new StoreWidget())->where('id',$widget_id)->update([
            'widget_type'=> $data['widget_type'],
            'widget_color'=> $data['widget_color'],
            'vertical_position'=> $data['vertical_position'],
            'horizontal_position'=> $data['horizontal_position'],
            'room_name' => empty($data['room_name']) ? 'room-' . $widget_id : $data['room_name'],
//            'logo_url'=> $fileName,
//            'generated_code'=> $data['generated_code'],
        ]);


        $fileName="";
        if($data['upload_file_changed']) {
            $uploadedFile = $request->file('logo');
            if (!empty($uploadedFile)) {
                $fileName = auth()->id() . '_' . time() . '.' . $uploadedFile->extension();
                if ($uploadedFile->isValid()) {
                    $uploadedFile->move(public_path('file'), $fileName);
                }
            }


            $fileName = '/file/' . $fileName;

            (new StoreWidget())->where('id',$widget_id)->update([
                'logo_url'=> $fileName,
            ]);
        }

        $fileName="";
        if($data['upload_file_2_changed']) {
            $uploadedFile = $request->file('waiting_image');
            if (!empty($uploadedFile)) {
                $fileName = auth()->id() . '_' . time() . '.' . $uploadedFile->extension();
                if ($uploadedFile->isValid()) {
                    $uploadedFile->move(public_path('file'), $fileName);
                }
            }
//            $fileName = '/file/' . $fileName;

            (new StoreWidget())->where('id',$widget_id)->update([
                'waiting_image'=> $fileName,
            ]);
        }

        return redirect('/admin/widgets/' . $store_id)->with('message', 'Widget updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
