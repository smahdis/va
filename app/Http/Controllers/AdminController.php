<?php

namespace App\Http\Controllers;

use App\Http\Controllers\API\TwilioController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class AdminController extends BaseController
{
    public function index(Request $request) {
        if(Auth::user()->isPromoter()) {
            return $this->promoterIndex($request);
        } else {
            return redirect('/admin');
        }
        return view('admin.index', ['test' => 'Testing']);
    }

    public function promoterIndex(Request $request)
    {
//        $rooms = Http::get('https://video.demos.ge/admin/widgets/getRooms/' . Auth::user()->store_id);
//        $current_room = Auth::user()->current_room;
//        return view('admin.index', ["rooms" => $rooms->object(), "current_room" => $current_room]);
        $is_promoting = Auth::user()->is_promoting;
        return view('admin.video.preview', ['widget_id' => Auth::user()->widget_id, "room_name" => Auth::user()->current_room, "is_promoting" => $is_promoting]);
    }

    public function preview(Request $request, $widget_id, $room_name)
    {
//        $token_info = (new TwilioController)->joinRoom($request);

        (new User())->where('id',Auth::user()->id)->update([
            'current_room'=> $room_name,
        ]);

        return view('admin.preview', ['widget_id' => $widget_id, "room_name" => $room_name]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function leaveRoom($room_name, $roomNameBase64=null)
    {
        (new User())->where('id',Auth::user()->id)->update([
            'is_promoting'=> null,
        ]);

//        var_dump($room_name);
//        die();
        if(isset($room_nameBase64)) {
            (new TwilioController())->leaveRoom($roomNameBase64);
        }

        return redirect('/')->with('message', 'You have successfully left ' . $room_name);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function connect(Request $request, $widget_id, $room_name)
    {
        (new User())->where('id',Auth::user()->id)->update([
            'is_promoting'=> 1,
        ]);
        $token_info = (new TwilioController)->joinRoom($request,$widget_id );


        return view('admin.video.connect', ["token_info" => $token_info, "room_name" => $room_name]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
