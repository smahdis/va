<?php

namespace App\Http\Controllers;

use App\Models\Store;
use App\Models\StoreWidget;
use App\Models\Timezone;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index($store_id)
    {
//        $widgets = StoreWidget::where('store_id', $store_id)->get();
        $users = User::where('store_id', $store_id)->get();
        $roles = (new User)->user_roles;
        return view('admin.user.index',["users" => $users, 'store_id' => $store_id, "roles" => $roles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create($store_id)
    {
//        $widget = new StoreWidget();
        $user = new User();
        $roles = $user->user_roles;
        $store = Store::where('store_id_pk', $store_id)->first();
//        $stores = (new Store)->where('status', 1)->get();

        DB::statement("SET SQL_MODE=''");
        $rooms = (new StoreWidget())
            ->where('status', 1)
            ->where('store_id', $store_id)
            ->groupBy('room_name')
            ->get();

//        var_dump($rooms);
//        die();

        return view('admin.user.create', ['store' => $store, 'store_id' => $store_id, "user" => $user, "roles" => $roles, "rooms" => $rooms]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function save(Request $request, $store_id)
    {
//        var_dump(Auth::id());
//        die();

        if(Auth::id() == 13) {
            $store = Store::where('store_id_pk', $store_id)->first();
            $data = $request->all();
            $user = new User();
            $user->first_name = $data['first_name'];
            $user->last_name = $data['last_name'];
            $user->full_name = $data['first_name'] . ' ' . $data['last_name'];
            $user->email = $data['email'];
            $user->store_id = $store_id;
            $user->store_title = $store->store_title;
            $user->password = Hash::make($data['password']);

            if(!empty($data['room_name'])) {
                $current_room = $data['room_name'] ? explode('|', $data['room_name']) : null;
                if($current_room[0]!=-1) {
                    $user->current_room = $data['room_name'] ? $current_room[1] : null;
                    $user->widget_id = $data['room_name'] ? $current_room[0] : null;
                }
            }

            $user->room_limit = (!empty($data['room_limit']) ? $data['room_limit']+1 : 50);

//            try {
                $user->save();
//            } catch (\Exception $e) {
                // do task when error
//            return redirect()->back()->with('error', 'A problem occured while saving the user, maybe email is duplicate')->withInput();
//                return Redirect::back()->withErrors(['error' => 'A problem occured while saving the user. Please check if you have entered all required fields. You might also check if a user has already registered with this address']);

//                echo $e->getMessage();   // insert query
//                return null;
//            }
//        Store::create($request->all());
            return redirect()->route('users', ["store_id" => $store_id]);
        } else {
            return response("You are not authorized", 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit($user_id)
    {
        if(Auth::id() == 13) {
            $user = (new User())->where('id', $user_id)->first();
            $stores = (new Store)->where('status', 1)->get();
            $store = (new Store)->where('store_id_pk', $user->store_id)->get();
            $roles = $user->user_roles;
            DB::statement("SET SQL_MODE=''");
            $rooms = (new StoreWidget())
                ->where('status', 1)
                ->where('store_id', $user->store_id)
                ->groupBy('room_name')
                ->get();

            return view('admin.user.create', ["user" => $user, "stores" => $stores, "store", $store, "roles" => $roles, "rooms" => $rooms]);
        } else {
            return response("You are not authorized", 200);
        }
    }


    public function updateBackground(Request $request)
    {

        $data = $request->all();



        $fileName="";
        $uploadedFile = $request->file('video_background_image');
        if (!empty($uploadedFile)) {
            $fileName = auth()->id() . '_' . time() . '.' . $uploadedFile->extension();
            if ($uploadedFile->isValid()) {
                $result1 = $uploadedFile->move(public_path('file'), $fileName);
//                var_dump($result1);
//                die();
            }
        }

        $result = User::where('id',Auth::id())->update([
            "video_background_type"      => $data['video_background_type']
        ]);

        if(isset($fileName) && !empty($fileName)) {
            $result = User::where('id',Auth::id())->update([
                "video_background_image"     => $fileName,
            ]);
        }
//        var_dump($result);
//        die();


        return $fileName;
//        return redirect('/admin/promoters')->with('message', 'Promoter updated successfully');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data = $request->all();
//        $store = explode('|', $data['store_id']);

        $current_room = [];
        if(!empty($data['room_name']) && isset($data['room_name'])) {
            $current_room = $data['room_name'] ? explode('|', $data['room_name']) : null;
        }

        User::where('id',$id)->update([
            "first_name"            => $data['first_name'],
            "last_name"             => $data['last_name'],
            "full_name"             => $data['first_name'] . ' ' . $data['last_name'],
            "email"                 => $data['email'],
            "current_room"          => (isset($current_room[1])) ? $current_room[1] : null,
            "widget_id"             => (isset($data['room_name'])) ? $current_room[0] : null,
//            "store_id"              => $store[0],
//            "store_title"           => $store[1],
            "room_limit"            => (!empty($data['room_limit']) ? $data['room_limit']+1 : 50)//$data['room_limit'],
        ]);

        if(!empty($data['password']) && isset($data['password'])) {
            User::where('id',$id)->update([
                "password"              => Hash::make($data['password'])
            ]);
        }

        $user = User::where('id',$id)->first();

        return redirect('/admin/users/' . $user->store_id)->with('message', 'User updated successfully');
//        return redirect('/admin/promoters')->with('message', 'Promoter updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        (new User())->where('id', $id)->delete();

        return redirect()->route('users');
    }
}
