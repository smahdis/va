
const container = document.getElementById("video-container");

const dataTrack = new Twilio.Video.LocalDataTrack();

let myLocalParticipant;
let room;


const startRoom = async (event) => {
    // prevent a page reload when a user submits the form
    if(event)
        event.preventDefault();

    const room = await joinVideoRoom(roomName, token);



    myLocalParticipant = room.localParticipant;

     // render the local and remote participants' video and audio tracks
    handleConnectedParticipant(room.localParticipant);
    room.participants.forEach(handleConnectedParticipant);

    room.on("participantConnected", handleConnectedParticipant);

    // handle cleanup when a participant disconnects
    room.on("participantDisconnected", handleDisconnectedParticipant);

    window.addEventListener("pagehide", () => room.disconnect());
    window.addEventListener("beforeunload", () => room.disconnect());

    // room.disconnect();

};

const handleConnectedParticipant = (participant) => {
    // create a div for this participant's tracks

    const participantDiv = document.createElement("div");
    const participantInfo = JSON.parse(participant.identity);
    participantDiv.setAttribute("id", participantInfo.id);
    if(participantInfo.is_promoter){
        participantDiv.className="participant-admin";
        container.prepend(participantDiv);
    } else {
        participantDiv.className="participant";
        container.querySelector('.participants-other').appendChild(participantDiv);
    }


    // iterate through the participant's published tracks and
    // call `handleTrackPublication` on them
    participant.tracks.forEach((trackPublication) => {
        handleTrackPublication(trackPublication, participant);
    });

    // listen for any new track publications
    participant.on("trackPublished", handleTrackPublication);

    participant.on('trackSubscribed', track => {
        // console.log(`Participant "${participant.identity}" added ${track.kind} Track ${track.sid}`);
        if (track.kind === 'data') {
            track.on('message', data => {
                console.log(data);
                $commands = JSON.parse(data);

                if($commands.uid){
                    console.log(myLocalParticipant);
                    // room.localParticipants.forEach((participant) => {
                    $uid = JSON.parse(myLocalParticipant.identity).id;
                    if($uid == $commands.uid) {

                        if($commands.type == "audio") {
                            myLocalParticipant.audioTracks.forEach(publication => {
                                if ( publication.track.isEnabled == true ) {
                                    publication.track.disable();
                                } else {
                                    publication.track.enable();
                                }
                            });
                        }

                        if($commands.type == "video") {
                            myLocalParticipant.videoTracks.forEach(publication => {
                                if ( publication.track.isEnabled == true ) {
                                    publication.track.disable();
                                } else {
                                    publication.track.enable();
                                }
                            });
                        }

                        if($commands.type == "kick") {
                            myLocalParticipant.tracks.forEach(publication => {
                                const attachedElements = publication.track.detach();
                                attachedElements.forEach(element => element.remove());
                            });
                            document.getElementById($uid).remove();
                            room.disconnect();
                            // alert("You have been kicked out");

                            container.innerHTML='<h3 style="grid-column: 3/5;text-align: center;">You have been kicked out</h3>';


                        }
                    }


                }

                // console.log(data);
            });
        }
    });

};


const handleTrackPublication = (trackPublication, participant) => {
    // const participantInfo = JSON.parse(participant.identity);
    function displayTrack(track) {

        if(participant) {
            const participantInfo = JSON.parse(participant.identity);
            const participantDiv = document.getElementById(participantInfo.id);

            // console.log(participantDiv);

            track.on('disabled', () => {
                /* Hide the associated <video> element and show an avatar image. */
                if(track.kind=="video") {
                    participantDiv.querySelector('video').style.display = "none";
                    participantDiv.querySelector('.mutevid').style.display = "block";
                }
                if(track.kind=="audio") {
                    participantDiv.querySelector('.mute').style.display = "block";
                    console.log("disabling mic for user: ", participantInfo);
                }
            });

            track.on('enabled', () => {
                if(track.kind=="video") {
                    participantDiv.querySelector('video').style.display = "block";
                    participantDiv.querySelector('.mutevid').style.display = "none";
                }
                if(track.kind=="audio") {
                    participantDiv.querySelector('.mute').style.display = "none";
                }
            });

            // append this track to the participant's div and render it on the page

            // track.attach creates an HTMLVideoElement or HTMLAudioElement
            // (depending on the type of track) and adds the video or audio stream

            if(track.kind=='audio') {
                const imgMute = document.createElement("img");
                imgMute.src = "https://virtual-assistants.gr/img/mute.png";
                imgMute.style = "width: 26px;position: relative;top: 30px;left: 5px;display: none;margin-top: -25px;background: #0c057a;padding: 5px;border-radius: 100%;";
                imgMute.className = "mute";
                participantDiv.prepend(imgMute);
                // if(participantInfo.is_promoter){
                if(participant.identity != myLocalParticipant.identity)
                    participantDiv.append(track.attach());
                if(!track.isEnabled){
                    imgMute.style = "width: 26px;position: relative;top: 30px;left: 5px;display: block;margin-top: -25px;background: #0c057a;padding: 5px;border-radius: 100%;"
                }
            }


            if(track.kind=='video') {
                if (typeof track.attach !== 'undefined') {
                    participantDiv.append(track.attach());

                }

                $participantFullName = participantInfo.full_name;

                const imgNoVideo = document.createElement("img");
                imgNoVideo.src = "https://virtual-assistants.gr/img/no-camera.png?v=234";
                imgNoVideo.style = "background-color: #36373a; border-radius: 10px; width: 100%;";

                if(participantInfo.is_promoter) {
                    imgNoVideo.style = "display:none;";
                    handleBackgroundProcessing(track, participantDiv);
                }

                if(track.isEnabled)
                    imgNoVideo.style =  "background-color: #36373a; border-radius: 10px; width: 100%;display:none;";

                if(!track.isEnabled)
                    participantDiv.querySelector('video').style.display = "none";

                imgNoVideo.className="mutevid";
                participantDiv.append(imgNoVideo);




                const participantLabel = document.createElement("label");
                if(participantInfo.is_promoter) {
                    participantLabel.style = 'text-align: center;background: rgba(190, 160, 160, 0.52);color: white;white-space: nowrap;padding: 5px 20px;position: relative;overflow: hidden;margin-top: -90px; float: left;';
                } else {
                    participantLabel.style = 'text-align: center;background: rgb(190 160 160 / 52%);color: white;white-space: nowrap;padding: 5px 20px;position: relative;bottom: 45px;';
                }
                participantLabel.innerHTML=$participantFullName;
                participantDiv.append(participantLabel);

                const controlDiv = document.createElement("div");
                controlDiv.style =  'display: grid; grid-template-columns: 1fr 1fr 1fr; text-align: center; margin-top: -40px;';
                controlDiv.innerHTML = '<a href="#" class="control-buttons mic-mute" data-uid="' + participantInfo.id +  '" style=""><i class="fa-solid fa-microphone"></i></a>' +
                    '<a href="#" class="control-buttons vid-mute" data-uid="' + participantInfo.id +  '" style=""><i class="fa-solid fa-video"></i></a>' +
                    '<a href="#" class="control-buttons kick" data-uid="' + participantInfo.id +  '" style=""><i class="fa-solid fa-right-from-bracket"></i></a>'
                ;
                participantDiv.append(controlDiv);


            }
        }
    }
    // check if the trackPublication contains a `track` attribute. If it does,
    // we are subscribed to this track. If not, we are not subscribed.
    if (trackPublication.track) {
        displayTrack(trackPublication.track);
    }

    // listen for any new subscriptions to this track publication
    trackPublication.on("subscribed", displayTrack);
};

const handleDisconnectedParticipant = (participant) => {
    const participantInfo = JSON.parse(participant.identity);
    console.log(participant.duration);
    // stop listening for this participant
    participant.removeAllListeners();
    // remove this participant's div from the page
    const participantDiv = document.getElementById(participantInfo.id);
    participantDiv.remove();
};

const joinVideoRoom = async (roomName, token) => {
    // join the video room with the Access Token and the given room name
    const tracks = await Twilio.Video.createLocalTracks(
        // {
        //     width: 640,
        //     height: 480,
        //     frameRate: 24
        // }
    );
    tracks.push(dataTrack);
    const room = await Twilio.Video.connect(token, {
        room: roomName,
        video: { height: 480, frameRate: 24, width: 640 },
        tracks: tracks,
    });
    return room;
};

// form.addEventListener("submit", startRoom);


