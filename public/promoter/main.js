const form = document.getElementById("videoChatForm");
const disconnect = window.document.getElementById("disconnect");
const gridContainer = document.getElementById("grid-container");

const formContainer = document.getElementById("videoChatContainer");
// const roomNameInput = document.getElementById("room-name-input");

const container = document.getElementById("video-container");

const enable_camera = document.getElementById('enable_camera');
const enable_mic = document.getElementById('enable_mic');

let myLocalParticipant;
let room;


const startRoom = async (event) => {
  // prevent a page reload when a user submits the form
  event.preventDefault();
    gridContainer.style.display="block";
  // hide the join form
    form.style.display = "none";
    formContainer.style.display = "none";
  // retrieve the room name
  // const roomName = roomNameInput.value;
  const firstNameInput = document.getElementById("fname");
  // const lastNameInput = document.getElementById("lname");
  let firstName = firstNameInput.value;
  // let lastName = lastNameInput.value;
  // fetch an Access Token from the join-room route
  const response = await fetch("https://virtual-assistants.gr/api/joinRoom/" + window.widget_id + "?v=556677", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      // "Authorization": "Bearer 2|JaXg5pCvSY5YNIB1prstQdGL1DKiXZz7fvwsid0y"
    },
    body: JSON.stringify({ firstName: firstName }),
  });




  const { token, roomName } = await response.json();

    if(!token) {
        $error_element = '<div style="text-align: center;padding: 20px;">' +
                                // '<label style="color: #fff; display: block;">Room is full or there are no promoters online, please try again later.</label>' +
                                '<img src="' + window.waiting_image_url + '" style="max-width: 400px; display: block; margin: 0 auto;">' +
                                '<button id="reloadButton" disabled onclick="window.location.reload();" style=" margin-top:20px; border-radius: 10px; background-color: #000000cc; border: none; color: white; padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; " disabled="">Reconnect</button>' +
                         '</div>';
        formContainer.style="display: unset;";

        formContainer.innerHTML = $error_element;
        timerEnableReloadButton(30);
        alert('There is no promoter online or room is full, please try again later');
        return;
    }



    disconnect.style.display = null;

  // console.log(token);
  // join the video room with the token
  room = await joinVideoRoom(roomName, token);

  myLocalParticipant = room.localParticipant;

    window.addEventListener('beforeunload', () => {
        room.disconnect();
    });

    console.log(enable_camera.checked);

if(!enable_camera.checked) {
    room.localParticipant.videoTracks.forEach(track => {
        // $muid = JSON.parse(myLocalParticipant.identity);
        //   console.log("$muid : ", $muid);
        //   console.log("$muid id: ", $muid.id);
        //   console.log("myLocalParticipant",myLocalParticipant);
        //   console.log(document.getElementById($muid.id));
        //
        // document.getElementById($muid.id).querySelector('video').style.display = "none";
        // document.getElementById($muid.id).querySelector('.mutevid').style.display = "block";

        track.track.disable();
    });
}


if(!enable_mic.checked) {

    room.localParticipant.audioTracks.forEach(track => {
        track.track.disable();
    });
}




  // render the local and remote participants' video and audio tracks
  handleConnectedParticipant(room.localParticipant);
  room.participants.forEach(handleConnectedParticipant);
  room.on("participantConnected", handleConnectedParticipant);

  // handle cleanup when a participant disconnects
  room.on("participantDisconnected", handleDisconnectedParticipant);
  window.addEventListener("pagehide", () => room.disconnect());
  window.addEventListener("beforeunload", () => room.disconnect());

disconnect.addEventListener("click", function(){
location.reload();
});

};

function timerEnableReloadButton ($timer) {
    // setTimeout(function(){},$timer*100);
    var intervalid = setInterval(function(){
        document.getElementById("reloadButton").innerHTML = "Reconnect in " + $timer;
        $timer--;

        if ($timer < 1) {
            clearInterval(intervalid);
            var btnReload = document.getElementById("reloadButton");
            btnReload.disabled = false;
            document.getElementById("reloadButton").innerHTML = "Reconnect";
            btnReload.style = " margin-top:20px; border: none; color: white; padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; background-color: #4CAF50; cursor: pointer;";
            // element.innerHTML = 'Time is up!';
            //document.getElementById("<%=btnPresentAnswer.ClientID %>").style.visibility = "visible"; // ← Uncomment this line!
        }

    },1000);
}

const handleConnectedParticipant = (participant) => {
  // create a div for this participant's tracks

  const participantDiv = document.createElement("div");
  const participantInfo = JSON.parse(participant.identity);
  participantDiv.setAttribute("id", participantInfo.id);
  if(participantInfo.is_promoter){
    participantDiv.className="participant-admin";
    container.prepend(participantDiv);
  } else {
    participantDiv.className="participant";
    container.querySelector('.participants-other').appendChild(participantDiv);
  }


  // iterate through the participant's published tracks and
  // call `handleTrackPublication` on them
  participant.tracks.forEach((trackPublication) => {
    handleTrackPublication(trackPublication, participant);
  });

  // listen for any new track publications
  participant.on("trackPublished", handleTrackPublication);

  participant.on('trackSubscribed', track => {
    // console.log(`Participant "${participant.identity}" added ${track.kind} Track ${track.sid}`);
    if (track.kind === 'data') {
      track.on('message', data => {
        // console.log(data);
        $commands = JSON.parse(data);

        if($commands.uid){
            console.log(myLocalParticipant);
          // room.localParticipants.forEach((participant) => {
            $uid = JSON.parse(myLocalParticipant.identity).id;
            if($uid == $commands.uid) {

              if($commands.type == "audio") {
                  myLocalParticipant.audioTracks.forEach(publication => {
                    if ( publication.track.isEnabled == true ) {
                      publication.track.disable();
                    } else {
                      publication.track.enable();
                    }
                  });
              }

              if($commands.type == "video") {
                myLocalParticipant.videoTracks.forEach(publication => {
                  if ( publication.track.isEnabled == true ) {
                    publication.track.disable();
                  } else {
                    publication.track.enable();
                  }
                });
              }

              if($commands.type == "kick") {
                myLocalParticipant.tracks.forEach(publication => {
                  const attachedElements = publication.track.detach();
                  attachedElements.forEach(element => element.remove());
                });
                document.getElementById($uid).remove();
                room.disconnect();
                // alert("You have been kicked out");

                  container.innerHTML='<h3 style="grid-column: 3/5;text-align: center;">You have been kicked out</h3>';


              }
            }


        }

        // console.log(data);
      });
    }
  });

};


const handleTrackPublication = (trackPublication, participant) => {
  // const participantInfo = JSON.parse(participant.identity);
  function displayTrack(track) {



    if(participant) {
        console.log("identity: ", participant.identity);

      const participantInfo = JSON.parse(participant.identity);
      const participantDiv = document.getElementById(participantInfo.id);

        track.on('disabled', () => {
            /* Hide the associated <video> element and show an avatar image. */
            if(track.kind=="video") {
                participantDiv.querySelector('video').style.display = "none";
                participantDiv.querySelector('.mutevid').style.display = "block";
            }
            if(track.kind=="audio") {
                participantDiv.querySelector('.mute').style.display = "block";
                console.log("disabling audio for: ", participantInfo.full_name);
            }
        });

        track.on('enabled', () => {
            if(track.kind=="video") {
                participantDiv.querySelector('video').style.display = "block";
                participantDiv.querySelector('.mutevid').style.display = "none";
            }
            if(track.kind=="audio") {
                participantDiv.querySelector('.mute').style.display = "none";
                console.log("enabling audio for: ", participantInfo.full_name);
            }
        });

      // append this track to the participant's div and render it on the page

      // track.attach creates an HTMLVideoElement or HTMLAudioElement
      // (depending on the type of track) and adds the video or audio stream

        if(track.kind=='audio') {
            const imgMute = document.createElement("img");
            imgMute.src = "https://virtual-assistants.gr/img/mute.png";
            imgMute.style = "width: 16px;position: relative;top: 30px;left: 5px; display: none;margin-top: -25px;background: #0c057a;padding: 5px;border-radius: 100%;";
            imgMute.className = "mute";

            // if(participantInfo.is_promoter){
            if(participant.identity != myLocalParticipant.identity)
                participantDiv.append(track.attach());
            if(!track.isEnabled){
                imgMute.style = "width: 16px;position: relative;top: 30px;left: 5px;display: block;margin-top: -25px;background: #0c057a;padding: 5px;border-radius: 100%;"
            }

            participantDiv.prepend(imgMute);
            // }
        }


      if(track.kind=='video') {
          if (typeof track.attach !== 'undefined') {
              participantDiv.append(track.attach());
          }

        // participantDiv.append(track.attach());
        $participantFullName = participantInfo.full_name;

          const imgNoVideo = document.createElement("img");
          imgNoVideo.src = "https://virtual-assistants.gr/img/no-camera.png?v=234";
          imgNoVideo.style = "background-color: #36373a; border-radius: 10px; width: 100%;";

          // imgNoVideo.style = "    width: 50px;\n" +
          //     "    background: url(https://video.demos.ge/img/no-camera.png);\n" +
          //     "    background-size: contain;\n" +
          //     "    background-repeat: no-repeat;\n" +
          //     "    background-position: center;\n" +
          //     "    height: 110px;\n" +
          //     "    margin: 0 auto;";
          if(participantInfo.is_promoter)
              imgNoVideo.style =  "display:none;";
          // } else {
          //     participantDiv.querySelector('video').style.display = "none";
          // }


          if(track.isEnabled)
              imgNoVideo.style =  "background-color: #36373a; border-radius: 10px; width: 100%;display:none;";

          // if(participant.identity == myLocalParticipant.identity || !track.isEnabled)
          if(!track.isEnabled)
              participantDiv.querySelector('video').style.display = "none";


          imgNoVideo.className="mutevid";
          participantDiv.append(imgNoVideo);




        const participantLabel = document.createElement("label");
        if(participantInfo.is_promoter) {
            participantLabel.style = 'text-align: center;background: rgba(190, 160, 160, 0.52);color: white;white-space: nowrap;padding: 5px 20px;position: relative;overflow: hidden;margin-top: -49px;float: left;';
        } else {
            participantLabel.style = 'text-align: center;background: rgb(190 160 160 / 52%);color: white;white-space: nowrap;padding: 5px 20px;position: relative;bottom: 45px;';
        }
        participantLabel.innerHTML=$participantFullName;
        participantDiv.append(participantLabel);


      }
    }
  }
  // check if the trackPublication contains a `track` attribute. If it does,
  // we are subscribed to this track. If not, we are not subscribed.
  if (trackPublication.track) {
    displayTrack(trackPublication.track);
  }

  // listen for any new subscriptions to this track publication
  trackPublication.on("subscribed", displayTrack);
};

const handleDisconnectedParticipant = (participant) => {
  const participantInfo = JSON.parse(participant.identity);
  // stop listening for this participant
  participant.removeAllListeners();
  // remove this participant's div from the page
  const participantDiv = document.getElementById(participantInfo.id);
  participantDiv.remove();
};

const joinVideoRoom = async (roomName, token) => {
  // join the video room with the Access Token and the given room name
  const room = await Twilio.Video.connect(token, {
    room: roomName,
  });
  return room;
};

form.addEventListener("submit", startRoom);


window.addEventListener('message', function(event) {
    $msg = event.data.message;
    if($msg=="closeIframe") {
        room.disconnect();
    }
}, false);
